//
// Created by houyx on 1/31/17.
//
#pragma once
#ifndef INC_2DINDEXTREE_YNODE_H
#define INC_2DINDEXTREE_YNODE_H

#include "YLnode.h"

class YLnode;

class YNode : public YLnode {

public:

    YNode( Storage& storage_, NodeId id ) : YLnode(storage_,v(yRoot),id)
    {}

    YNode( Storage& storage_, NodeId id, int mark ) : YLnode(storage_,id,1)
    {}

    YNode( Storage& storage_, Object object ): YLnode(storage_,v(yRoot),object)
    {
    }

    YNode(const YNode& c4):YLnode(c4.storage,c4.id,1)
    {}

    YNode& operator=(const YNode&c4)
    {
        id = c4.id;
        belongTo = c4.belongTo;
        return *this;
    }

    bool operator==(const YNode &c4);

    int getHeight();
    bool setHeight(int h);

    YNode getLeftChild();
    YNode getRightChild();
    YNode getParent();

    bool setLeftChild( YNode& a );
    bool setRightChild( YNode& a );
    bool setParent( YNode& a );

    bool hasLeftChild();
    bool hasRightChild();
    bool hasParent();

    bool isLeftChild();
    bool isRightChild();
    bool isRoot();


};


#endif //INC_2DINDEXTREE_YNODE_H
