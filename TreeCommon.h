//
// Created by houyx on 1/25/17.
//
#pragma once
#ifndef INC_2DINDEXTREE_TREECOMMON_H
#define INC_2DINDEXTREE_TREECOMMON_H

#include "XNode.h"
#include "YNode.h"
#include "YLnode.h"
#include "YListCommon.h"

class YList;
class YLnode;

bool balanced(XNode& g);

void updateHeight(XNode& g);

XNode connect34(XNode &a, XNode &b, XNode &c, XNode t0, XNode t1, XNode t2, XNode t3, YList gYList);


XNode rotateAt( XNode a );

XNode tallerChild( XNode a );

void fixListPointer( XNode& w,XNode& succ );

void swapDataX( XNode& x, XNode& w );

void deleteWFromWtoX(XNode &x, XNode &w);

XNode findNext(XNode a);

XNode findPre(XNode a);

#endif //INC_2DINDEXTREE_TREECOMMON_H
