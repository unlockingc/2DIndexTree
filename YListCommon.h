//
// Created by houyx on 2/4/17.
//

#ifndef INC_2DINDEXTREE_YLISTCOMMON_H
#define INC_2DINDEXTREE_YLISTCOMMON_H

#include "YList.h"

class YList;

void fixYList( YList& lp, YList ll,  YList lr );

void merge(YList l1, YList l2, XNode &belongTo, Object obj);

#endif //INC_2DINDEXTREE_YLISTCOMMON_H
