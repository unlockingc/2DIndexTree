//
// Created by houyx on 1/22/17.
//
#pragma once
#ifndef INC_2DINDEXTREE_XNODE_H
#define INC_2DINDEXTREE_XNODE_H


#include "Node.h"


using namespace std;

static int YListHeader, YListTail;

/***
 * use hashset in redis to storage node
 */
class XNode :public Node
{

public:

    XNode( Storage& storage_, string key_):Node( storage_,key_ )
    {}


    XNode( Storage& storage_, Object object );
    XNode(const XNode& c4):Node(c4.storage,c4.id)
    {
    }

    XNode& operator=(const XNode& c4)
    {
        id = c4.id;

        return *this;
    }

    bool operator==(const XNode &c4);

    Object getObject();

    int getHeight();
    XNode getLeftChild();
    XNode getRightChild();
    XNode getParent();

    double getX();
    double getY();

    bool setLeftChild( XNode& a );
    bool setRightChild( XNode& a );
    bool setParent( XNode& a );
    bool setHeight(int h);


    bool isLeftChild();
    bool isRightChild();
    bool isRoot();

    bool hasLeftChild();
    bool hasRightChild();
    bool hasParent();

    bool setYTail(NodeId Yid);

    bool setYHeader(NodeId yId);

    NodeId getYTail();

    NodeId getYHeader();

    NodeId getYListMark();

    bool setYListMark(NodeId mark);
};


#endif //INC_2DINDEXTREE_XNODE_H
