
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Storage.h"
#include "TreeInterface.h"
//#include "TestClass.h"
#include "TestCase.h"


#define v(x) #x

/*#define REDIS_REPLY_STRING 1
#define REDIS_REPLY_ARRAY 2
#define REDIS_REPLY_INTEGER 3
#define REDIS_REPLY_NIL 4
#define REDIS_REPLY_STATUS 5
#define REDIS_REPLY_ERROR 6*/

using namespace std;

void stringTest( string a )
{
    std::cout << a << " xxx" << endl;
}

class TestClass1
{
public:

    void func11()
    {
        cout << "fun11" <<endl;
    }

    void func12()
    {
        cout << "fun12" << endl;
    }
};

class TestClass2 : public TestClass1
{
public:
    void func21()
    {
        cout << "fun21" << endl;
    }
    void func22()
    {
        cout << "fun22" << endl;
    }
};


class TT
{
public:
    TestClass1& a,b;
    TT(TestClass1& a_, TestClass1& b_):a(a_),b(b_)
    {}
};






int main(int argc, char **argv) {

    //toDebug();
    assert(argc == 3);
    string a1 = argv[1];
    string a2 = argv[2];
/*    string a3 = argv[3];
    string a4 = argv[4];
    string a5 = argv[5];
    string a6 = argv[6];
    string a7 = argv[7];
    cout << a1 << "," << a2 <<"," << a3 << "," << a4 << endl;*/

    int ai1= atoi(a1.c_str());/*,ai2 = atoi(a2.c_str()),ai3 = atoi(a3.c_str()),ai4 = atoi(a4.c_str()),ai6 = atoi(a6.c_str());*/
    //float ai5 = atof(a5.c_str());

    //void test2(int beseType, int baseNumber, int endNumber, int incType, double step, int times, string testId)
    cout << ai1 << "," << a2 << endl;


    testFromInitSuq(ai1, a2 );
    //test2(ai1,ai2,ai3,ai4,ai5,ai6,a7);
/*    struct timezone tz;
    struct timeval tt1, tt2;
    timespec t1,t2;
    gettimeofday(&tt1,&tz);
    clock_gettime(CLOCK_MONOTONIC,&t1);
    //toDebug();
    //sleep(3);
    clock_gettime(CLOCK_MONOTONIC,&t2);
    gettimeofday(&tt2,&tz);
    OUT(cout) << "time1: " << t2.tv_nsec - t1.tv_nsec << " u: "  << tt2.tv_sec - tt1.tv_sec<< endl;


    OUT(cout) << "time4: " << t2.tv_sec - t1.tv_sec << " u: "  << tt2.tv_usec - tt1.tv_usec<< endl;

    gettimeofday(&tt1,&tz);
    clock_gettime(CLOCK_MONOTONIC,&t1);
    //usleep(2000);
    sleep(10);
    clock_gettime(CLOCK_MONOTONIC,&t2);
    gettimeofday(&tt2,&tz);
    cout << "time2: " << t2.tv_nsec - t1.tv_nsec <<  " u: "  << tt2.tv_sec - tt1.tv_sec<< endl;

    cout << "time3: " << t2.tv_sec - t1.tv_sec << " u: "  << tt2.tv_usec - tt1.tv_usec<< endl;*/
 /*   unsigned int j;
    redisContext *c;
    redisReply *reply;
    const char *hostname = (argc > 1) ? argv[1] : "127.0.0.1";
    int port = (argc > 2) ? atoi(argv[2]) : 6379;

    struct timeval timeout = { 1, 500000 }; // 1.5 seconds
    c = redisConnectWithTimeout(hostname, port, timeout);
    if (c == NULL || c->err) {
        if (c) {
            printf("Connection error: %s\n", c->errstr);
            redisFree(c);
        } else {
            printf("Connection error: can't allocate redis context\n");
        }
        exit(1);
    }



    *//* PING server *//*
    reply = (redisReply *)  redisCommand(c,"PING");
    printf("PING: %s\n", reply->str);
    freeReplyObject(reply);


    string zz = "0123456";
    cout << "zz\n";
    cout << zz.find("1") << endl;
    cout << zz.find("9") << " " << (int)(zz.find("9"))<< endl;
    cout << zz.substr(-1,2) << endl;

    cout << "incr :" <<endl;
    reply = (redisReply *) redisCommand(c,"INCR BBCC");
    printf("SET: %d %lld\n", reply->type,reply->integer);
    freeReplyObject(reply);

    *//* Set a id *//*
    cout << "hset :" <<endl;
    reply = (redisReply *) redisCommand(c,"HSET houyx name williamhou");
    printf("SET: %d %lld\n", reply->type,reply->integer);
    freeReplyObject(reply);



    *//* Set a id *//*
    cout << "hget :" <<endl;
    reply = (redisReply *) redisCommand(c,"HGET houyx name");
    printf("SET: %d %s\n", reply->type,reply->str);
    freeReplyObject(reply);
    *//* Set a id *//*
    cout << "hget1 :" <<endl;
    reply = (redisReply *) redisCommand(c,"HGET houyx name1");
    printf("SET: %d %s\n", reply->type,reply->str );
    if( reply->str == NULL )
    {
        printf("yes it is~");
    }
    freeReplyObject(reply);

    *//* Set a id *//*
    reply = (redisReply *) redisCommand(c,"SET %s %s", "foo", "hello world");
    printf("SET: %s\n", reply->str);
    freeReplyObject(reply);

    *//* Set a id *//*
    reply = (redisReply *) redisCommand(c,"SET %s %s", "foo", "hello world");
    printf("SET: %s\n", reply->str);
    freeReplyObject(reply);

    *//* Set a id using binary safe API *//*
    reply =  (redisReply *) redisCommand(c,"SET %b %b", "bar", (size_t) 3, "hello", (size_t) 5);
    printf("SET (binary API): %s\n", reply->str);
    freeReplyObject(reply);

    *//* Try a GET and two INCR *//*
    reply = (redisReply *)  redisCommand(c,"GET foo");
    printf("GET foo: %s\n", reply->str);
    freeReplyObject(reply);

    reply = (redisReply *)  redisCommand(c,"INCR counter");
    printf("INCR counter: %lld\n", reply->integer);
    freeReplyObject(reply);
    *//* again ... *//*
    reply = (redisReply *)  redisCommand(c,"INCR counter");
    printf("INCR counter: %lld\n", reply->integer);
    freeReplyObject(reply);

    *//* Create a list of numbers, from 0 to 9 *//*
    reply = (redisReply *)  redisCommand(c,"DEL mylist");
    freeReplyObject(reply);
    for (j = 0; j < 10; j++) {
        char buf[64];

        snprintf(buf,64,"%u",j);
        reply = (redisReply *)  redisCommand(c,"LPUSH mylist element-%s", buf);
        freeReplyObject(reply);
    }

    *//* Let's check what we have inside the list *//*
    reply = (redisReply *)  redisCommand(c,"LRANGE mylist 0 -1");
    if (reply->type == REDIS_REPLY_ARRAY) {
        for (j = 0; j < reply->elements; j++) {
            printf("%u) %s\n", j, reply->element[j]->str);
            stringTest(reply->element[j]->str);
        }
    }
    freeReplyObject(reply);

    *//* Disconnects and frees the context *//*
    redisFree(c);*/

    cout << "I am running" << endl;

    return 0;
}