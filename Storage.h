//
// Created by houyx on 1/23/17.
//
#pragma once
#ifndef INC_2DINDEXTREE_STORAGE_H
#define INC_2DINDEXTREE_STORAGE_H


#include <stdlib.h>
#include <algorithm>
#include <iostream>
#include <vector>
#include <cstring>
#include <string.h>
#include <assert.h>
#include <hiredis/hiredis.h>
#include "Type.h"
#include "Node.h"

using namespace std;

class Node;

class Storage
{
private:
    redisContext *c;
    redisReply *reply;
    char* hostname;
    int port;


    struct timeval timeout; // 1.5 seconds

public:
    Storage( char* hostname_ =  "127.0.0.1", int port_ = 6379 ): hostname(hostname_), port(port_)
    {
        //todo: understand timeout and find a suitable value
        timeout = { 1, 500000 };

        c = redisConnect(hostname, port);
        if (c == NULL || c->err) {
            if (c) {
                printf("Connection error: %s\n", c->errstr);
                redisFree(c);
            } else {
                printf("Connection error: can't allocate redis context\n");
            }
            exit(1);
        }

    }

    bool saveNode( Node node );

    Node loadNode( string key );

    bool deleteNode( string key );

    bool updateFieldOfNode( string key, string field, string value );

    bool deleteFieldOfNode(string key, string field);

    string getFieldOfNode( string key, string field );

    Object getObjectFromY( string key );

    bool setObjectFromY(Object& object, NodeId belongTo);

    bool flushAll();

    string getUuid();
};

#endif //INC_2DINDEXTREE_STORAGE_H
