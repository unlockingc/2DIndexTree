//
// Created by houyx on 1/23/17.
//
#pragma once
#ifndef INC_2DINDEXTREE_XTREE_H
#define INC_2DINDEXTREE_XTREE_H
#include "YTree.h"
#include "YList.h"

using namespace std;

class XTree {

private:
    Storage& storage;
    XNode root;
    XNode _hot;
    XNode _searchIn( XNode& rootIn, XNode& id, XNode& _hot );
    XNode _search( XNode& newNode );
    void _insertInYList(Neighbor &neighbor, Object &object);
    void _insertInYListIn( XNode& now, Neighbor& neighbor, Object& object, YList& list );
    void _removeInYList(Neighbor &neighbor, Object &object);
    void _removeInYListIn(XNode &now, Neighbor &neighbor, Object &object, YList &list);
    YTree yRoot;


    void
    _rangeSearchTogether(YLnode &yMin, YLnode &yMax, Object &min, Object &max, XNode &rootIn, vector<Object> &result);

    void _rangeSearchInMinPath(YLnode &yMin, YLnode &yMax, vector<Object> &result, XNode &rootIn, Object &min, Object &max);
    void _rangeSearchInMaxPath(YLnode &yMin, YLnode &yMax, vector<Object> &result, XNode &rootIn, Object &min, Object &max);

public:
    //init ytree
    XTree( Storage& storage_ ) : storage(storage_), root(storage,Object("initRoot",0,0,"useless init XNode") ),_hot(root),yRoot(storage_,root)
    {
        root.setYListMark("yRoot");
        root.setYHeader("yRoot:initRoot");
        root.setYTail("yRoot:initRoot");
    }

    Object insert( Object& object );

    bool remove( NodeId& id );

    void removeAt( XNode& x, XNode& parent );

    //Object search( NodeId& id );

    vector<Object> range( Range& range );
};


#endif //INC_2DINDEXTREE_XTREE_H
