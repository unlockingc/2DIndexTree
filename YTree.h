//
// Created by houyx on 1/23/17.
//
#pragma once
#ifndef INC_2DINDEXTREE_YTREE_H
#define INC_2DINDEXTREE_YTREE_H

#include "YTreeCommon.h"

class YTree {

private:
    Storage& storage;
    YNode root;
    YNode _hot;
    YNode _searchIn( YNode& rootIn, YNode& id, YNode& _hot );
    YNode _search( YNode& newNode );

public:
    XNode belongTo;
    //init ytree
    /***
     * todo: fix space wrong problem
     * @param storage_
     * @param belongTo_
     */
    YTree( Storage& storage_,XNode& belongTo_ ) : storage(storage_), root(storage,Object("initRoot",0,0,"\"uselessInitNode\"") ),_hot(root), belongTo(belongTo_)
            {
            }

    Neighbor insert(Object &object);

    Neighbor remove(NodeId &id);

    Neighbor removeAtY(YNode &x, YNode &_hot);

    //Object search( NodeId& id );

    vector<Object> range( Range& range );

    YLnode getYLess(Object &min, Object &max);
    YLnode getYBigger(Object &min, Object &max);
};


#endif //INC_2DINDEXTREE_YTREE_H
