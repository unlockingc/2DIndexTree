//
// Created by houyx on 1/31/17.
//

#include "Node.h"


bool Node::isNull() {
    return id == NULL_REDIS;
}

Node::Node( Storage& storage_, string key_ ): id(key_), storage(storage_)
{
    //values.clear();
}

string Node::getValue( string field )
{
    opCountLoad ++;
    return storage.getFieldOfNode(id, field);
}

bool Node::setValue( string field, string value )
{
/*    ssmap::iterator it = values.find(field);

    if( it == values.end() )
    {
        values.insert(pair<string,string>(field,value));
    }
    else
    {
        it->second = value;
    }*/
    opCountSet++;
    return storage.updateFieldOfNode(id, field, value);
}

void Node::loadValue(string field, string value)
{
    //values.insert(pair<string,string>(field,value));
}

void Node::removeValue( string field )
{
    //values.erase(field);
    storage.deleteFieldOfNode(id, field);
}
/*
string Node::toString()
{
    *//*
    this->SendMsg(iter->second.pSocket,strMsg);//iter->first*//*
    string ret=" " + id + " ";
    ssmap::iterator iter;
    for(iter = values.begin(); iter != values.end(); ++iter)
    {
        ret += iter->first + " " + iter->second + " ";
    }

    return ret;
}*/

bool Node::operator==(const Node &c4)
{
    return id==c4.id;
}

bool Node::deleteSelf() {
    return storage.deleteNode(id);
}