//
// Created by houyx on 4/19/17.
//

#include "TestCase.h"
#include "math.h"

const int MAX = 10000000;
const int repeat = 10;
bool xNumbers[MAX];
bool yNumbers[MAX];

void initTreeSuq(TreeInterface& anInterface, int baseNumber);
void initTreeRand(TreeInterface& anInterface, int baseNumber);
void initTreeLined(TreeInterface& anInterface, int baseNumber);
void initTreeLinedVec(TreeInterface& anInterface, int baseNumber);

void initTreeSuq2(TreeInterface &anInterface, int baseNumber, int endNumber, int lastNumber, int currentNumber);

string intToString(int num)
{
    stringstream ss;
    ss<<num;   //像流中传值
    string result;
    ss>>result;  //将流中的值写入到result
    return result;
}


void toDebug() {

    Storage storage;
    storage.flushAll();
    TreeInterface test(storage);

    Object obj1("foo1",1,1,"ww");
    Object obj2("foo2",2,2,"ww");
    Object obj3("foo3",3,3,"ww");
    Object obj4("foo4",4,4,"ww");
    Object obj5("foo5",5,5,"ww");
    Object obj6("foo6",6,6,"ww");
    Object obj7("foo7",7,7,"ww");
    Object obj8("foo8",8,8,"ww");

    test.insert(obj1);
    test.insert(obj2);
    test.insert(obj3);
    test.insert(obj4);
    test.insert(obj5);
    test.insert(obj6);
    test.insert(obj7);
    test.insert(obj8);

    cout << "inserted" << endl;
    Range range(0,10,0,10);
    vector<Object> rangeResult = test.rangeQuery(range);

    test.remove(obj1.id);
    test.remove(obj2.id);
    test.remove(obj3.id);
    test.remove(obj4.id);
    test.remove(obj5.id);
    test.remove(obj6.id);
    test.remove(obj7.id);
    test.remove(obj8.id);

/*    for(int j = 8; j < 65; j=j*2 ) {

        Storage storage;
        storage.flushAll();
        TreeInterface test(storage);
        string input = "as";
        cout << j << " started " << test.search(input).content << endl;

        for (int i = 1; i < j; i++) {
            Object obj1("lala" + intToString(i), i, i, "ww");
            test.insert(obj1);
            cout << i << " inserted" << endl;
        }

        cout << "inserted" << endl;


        for (int i = j-1; i >= 1; i--) {
            Object obj1("lala" + intToString(i), i, i, "ww");
            test.remove(obj1.id);

            cout << i << " removed" << endl;
        }

        cout << j << " finished!!!!!!!!!!" << endl;
    }*/


}


#define OUT(a) a << beseType << "," << lastNumber << "," << times << ","
#define OUTTIME(a,count) a << beseType << "," << lastNumber << "," << times << "," << count << "," << (tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec) << "," << opCountLoad << "," << opCountSet<< endl
#define OUTTIME1(a,avarage) a << beseType << "," << lastNumber << "," << times << "," << avarage << ","  << opCountLoad << "," << opCountSet<< endl

/***
 *
 * @param beseType 0:sequence 1: random 2: lined 3: vecLined 4: squred
 * @param baseNumber
 * @param times
 */
/*
void test(int beseType, int baseNumber, int times, string testId) {
    struct timezone tz;
    struct timeval tt1, tt2;
    string gobo = "total";

    ofstream outI,outD,outR,outC;
    ofstream outIa,outDa,outRa,outCa;
    outI.open(testId+"_insert"+".csv",ios::ate);
    outD.open(testId+"_delete"+".csv",ios::ate);
    outR.open(testId+"_Range"+".csv",ios::ate);
    outC.open(testId+"_Create"+".csv",ios::ate);

    outIa.open(gobo + "_insert"+".csv",ios::app);
    outDa.open(gobo + "_delete"+".csv",ios::app);
    outRa.open(gobo + "_Range"+".csv",ios::app);
    outCa.open(gobo + "_Create"+".csv",ios::app);

    if(!outI)
    {
        cout << "hehehehehehehehehehehehehehehehe" << endl;
        return;
    }
    outI << "test!!!\n";

    Storage storage;
    storage.flushAll();
    TreeInterface test(storage);



    srand((unsigned)time(0));

    memset(xNumbers,0,MAX*sizeof(bool));
    memset(yNumbers,0,MAX*sizeof(bool));

    opCountLoad = 0;
    opCountSet = 0;
    // init tree
    cout <<"init started" << endl;
    gettimeofday(&tt1,&tz);
    switch(beseType)
    {
        case 0:
            initTreeSuq(test,baseNumber);
            break;
        case 1:
            initTreeRand(test,baseNumber);
            break;
        case 2:
            initTreeLined(test,baseNumber);
            break;
        case 3:
            initTreeLinedVec(test,baseNumber);
            break;
        case 4:
        default:
            initTreeSuq(test,baseNumber);
            break;
    }
    gettimeofday(&tt2,&tz);
    OUTTIME(outC,0);
    OUTTIME(outCa,0);
    //OUTTIME(cout, 0);
    //OUT(outC) << (tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec) << endl;

    cout << "init ended" << endl;

    cout << "update test started" << endl;
    //test update
    int x=1,y=1;
    long long sumI = 0, sumD = 0, sumR = 0;
    for( int j = 0; j < times; j ++ )
    {
        int xNumbersTemp[repeat];
        int yNumbersTemp[repeat];
        Object objs[repeat];
        memset(xNumbersTemp,0,repeat*sizeof(int));
        memset(xNumbersTemp,0,repeat*sizeof(int));

        // init data
        for( int i = 0; i < repeat; i++ )
        {
            while( xNumbers[x] == true  )
            {
                x = rand()%MAX;
            }

            while( yNumbers[y] == true )
            {
                y = rand()%MAX;
            }

            xNumbers[x] = true;
            yNumbers[y] = true;
            xNumbersTemp[i] = x;
            yNumbersTemp[i] = y;

            objs[i].id = intToString(x) + "P" + intToString(y);
            objs[i].x = x;
            objs[i].y = y;
        }


        opCountLoad = 0;
        opCountSet = 0;
        //time
        gettimeofday(&tt1,&tz);
        for( int i = 0; i < repeat; i ++ )
        {
            test.insert(objs[i]);
        }
        gettimeofday(&tt2,&tz);
        OUTTIME(outI,j);
        //outI << beseType << "," << baseNumber << "," << times << "," << j << "," << (tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec) << "," << opCountLoad << "," << opCountSet<< endl;

        //OUTTIME(cout,j);
        sumI+=((tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec));


*/
/*        int temp;
        gettimeofday(&tt1,&tz);
        Range range(x-1,x+1024,y-1,y+1024);
        for( int k = 0; k < repeat; k ++ )
        {
            temp = test.rangeQuery(range).size();
        }
        gettimeofday(&tt2,&tz);
        OUT(outR) << j << "," << (tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec) << "," << temp << endl;
        sumR += ((tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec));*//*




        opCountLoad = 0;
        opCountSet = 0;
        gettimeofday(&tt1,&tz);
        for( int i = 0; i < repeat; i ++ )
        {
            test.remove(objs[i].id);
        }
        gettimeofday(&tt2,&tz);
        OUTTIME(outD,j);
        //OUTTIME(cout,j);
        sumD+=((tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec));
        //finalize
        for( int i = 0; i < repeat; i ++ )
        {
            xNumbers[xNumbersTemp[i]] = false;
            yNumbers[yNumbersTemp[i]] = false;
        }
    }

    OUTTIME(outIa,sumI/times);
    OUTTIME(outDa,sumD/times);
    OUTTIME(outRa,sumR/times);

    cout << "range test ended" << endl;

    storage.flushAll();
    outD.close();
    outI.close();
    outR.close();
    outDa.close();
    outIa.close();
    outRa.close();

}
*/

void test2(int beseType, int baseNumber, int endNumber, int incType, double step, int times, string testId) {

    struct timezone tz;
    struct timeval tt1, tt2;
    string gobo = "total";

    ofstream outI,outD,outR,outC;
    ofstream outIa,outDa,outRa,outCa;
    outI.open(testId+"_insert"+".csv",ios::ate);
    outD.open(testId+"_delete"+".csv",ios::ate);
    outR.open(testId+"_range"+".csv",ios::ate);

    outIa.open(gobo + "_insert"+".csv",ios::app);
    outDa.open(gobo + "_delete"+".csv",ios::app);
    outRa.open(gobo + "_range"+".csv",ios::app);
    if(!outI || !outD || !outR || !outIa || !outDa || !outRa)
    {
        cout << "hehehehehehehehehehehehehehehehe" << endl;
        return;
    }
    outI << "test!!!\n";

    Storage storage;
    storage.flushAll();
    TreeInterface test(storage);



    srand((unsigned)time(0));

    memset(xNumbers,0,MAX*sizeof(bool));
    memset(yNumbers,0,MAX*sizeof(bool));

    opCountLoad = 0;
    opCountSet = 0;
    // init tree


    int lastNumber = 0,currentNumber = baseNumber;

/*
    int lastNubmer = (step == 0)? 0 : ((incType == 0)? (int)((double)baseNumber*pow((double)incNumber,(double)(step - 1))):baseNumber+incNumber*(step-1));
    int currentNumber = ((incType == 0)? (int)((double)baseNumber*pow((double)incNumber,(double)(step))):baseNumber+incNumber*(step));*/


    while( currentNumber <= endNumber  )
    {
        cout << currentNumber << " from " << baseNumber << " to " << endNumber << endl;
        initTreeSuq2(test,baseNumber,endNumber,lastNumber,currentNumber);
        cout << "inited" <<endl;
        lastNumber = currentNumber;
        currentNumber = (incType == 0)? (int)((double)(currentNumber)*step): (int)(currentNumber + step);
        cout << "update test started" << endl;

        //test update
        int x=1,y=1;
        long long sumI = 0, sumD = 0, sumR = 0;
        for( int j = 0; j < times; j ++ )
        {
            int xNumbersTemp[repeat];
            int yNumbersTemp[repeat];
            Object objs[repeat];
            memset(xNumbersTemp,0,repeat*sizeof(int));
            memset(xNumbersTemp,0,repeat*sizeof(int));

            // init data
            for( int i = 0; i < repeat; i++ )
            {
                while( xNumbers[x] == true  )
                {
                    x = rand()%MAX;
                }

                while( yNumbers[y] == true )
                {
                    y = rand()%MAX;
                }

                xNumbers[x] = true;
                yNumbers[y] = true;
                xNumbersTemp[i] = x;
                yNumbersTemp[i] = y;

                objs[i].id = intToString(x) + "P" + intToString(y);
                objs[i].x = x;
                objs[i].y = y;
            }


            opCountLoad = 0;
            opCountSet = 0;
            //time

            cout << "times: " << j << " insert start" << endl;
            gettimeofday(&tt1,&tz);
            for( int i = 0; i < repeat; i ++ )
            {
                test.insert(objs[i]);
            }
            gettimeofday(&tt2,&tz);
            OUTTIME(outI,j);
            //outI << beseType << "," << baseNumber << "," << times << "," << j << "," << (tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec) << "," << opCountLoad << "," << opCountSet<< endl;


            cout << "times: " << j << " insert end range start" << endl;
            //OUTTIME(cout,j);
            sumI+=((tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec));


            opCountLoad = 0;
            opCountSet = 0;
            int temp;
            gettimeofday(&tt1,&tz);
            Range range(x-1,x+1024,y-1,y+1024);
            for( int k = 0; k < repeat; k ++ )
            {
                temp = test.rangeQuery(range).size();
            }
            gettimeofday(&tt2,&tz);
            OUT(outR) << j << "," << (tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec) << "," << opCountLoad << "," << opCountSet << "," << temp << endl;
            sumR += ((tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec));
            cout << "times: " << j << " range end delete start" << endl;

            opCountLoad = 0;
            opCountSet = 0;
            gettimeofday(&tt1,&tz);
            for( int i = 0; i < repeat; i ++ )
            {
                test.remove(objs[i].id);
            }
            gettimeofday(&tt2,&tz);
            OUTTIME(outD,j);

            cout << "times: " << j << " delete end" << endl;
            //OUTTIME(cout,j);
            sumD+=((tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec));
            //finalize
            for( int i = 0; i < repeat; i ++ )
            {
                xNumbers[xNumbersTemp[i]] = false;
                yNumbers[yNumbersTemp[i]] = false;
            }

        }
        cout << "update ended" << endl;
        OUTTIME1(outIa,sumI/times);
        OUTTIME1(outDa,sumD/times);
        OUTTIME1(outRa,sumR/times);
    }

    cout << "all ended" << endl;

    storage.flushAll();
    outD.close();
    outI.close();
    outR.close();
    outDa.close();
    outIa.close();
    outRa.close();

}


void testFromInit(int endNumber, string testId) {

    struct timezone tz;
    struct timeval tt1, tt2;
    string gobo = "total";

    ofstream outIa,outDa,outRa;

    outIa.open(gobo + "_insert"+".csv",ios::app);
    outDa.open(gobo + "_delete"+".csv",ios::app);
    outRa.open(gobo + "_range"+".csv",ios::app);
    if( !outIa || !outDa || !outRa)
    {
        cout << "hehehehehehehehehehehehehehehehe" << endl;
        return;
    }


    Storage storage;
    storage.flushAll();
    TreeInterface test(storage);



    srand((unsigned)time(0));

    memset(xNumbers,0,MAX*sizeof(bool));
    memset(yNumbers,0,MAX*sizeof(bool));


    // init tree
    const int STEP = 10;

    int x=1,y=1;
    int inv = (MAX/endNumber)*STEP;
    int invCount = MAX/inv;
    assert( (MAX/endNumber)*STEP >STEP*3 );

    Range range(1,2,1,2);
    Object objTemp("z",1,1,"w");
    int xBase = inv*( rand()%invCount );
    int yBase = inv*( rand()%invCount );
    int NumberCurrent = 0;

    int* xtoRemove = new int[STEP*invCount];
    int* ytoRemove = new int[STEP*invCount];
    memset(xtoRemove,0,STEP*invCount*sizeof(int));
    memset(ytoRemove,0,STEP*invCount*sizeof(int));
    int rangeCount;

    cout << "insert and range start" << endl;

    for( int i = 0; i < invCount; i++ )
    {
        if(i%10 == 0)
        {
            cout << "Insert " << NumberCurrent << endl;
        }

        //select inv
        while( xNumbers[xBase] == true )
        {
            xBase = inv*( rand()%invCount );
        }

        while( yNumbers[yBase] == true )
        {
            yBase = inv*(rand()%invCount);
        }

        xNumbers[xBase] = true;
        yNumbers[yBase] = true;

        x = xBase + rand()%inv;
        y = yBase + rand()%inv;
        range.xMin = x;
        range.xMax = x + 1;
        range.yMin = y;
        range.yMax = y + 1;

        for( int j = 0; j < STEP; j ++ )
        {


            while( xNumbers[x] == true )
            {
                x = xBase + rand()%inv;
            }

            while( yNumbers[y] == true )
            {
                y = yBase + rand()%inv;
            }

            xtoRemove[NumberCurrent] = x;
            ytoRemove[NumberCurrent] = y;

            xNumbers[x] = true;
            yNumbers[y] = true;

            if( range.xMin > x )
            {
                range.xMin = x ;
            }
            if( range.yMin > y)
            {
                range.yMin = y;
            }

            if( range.xMax < x + 1 )
            {
                range.xMax = x + 1;
            }

            if( range.yMax < y + 1 )
            {
                range.yMax = y + 1;
            }

            objTemp.id = intToString(x) + "P" + intToString(y);
            objTemp.x = x;
            objTemp.y = y;

            opCountLoad = 0;
            opCountSet = 0;
            gettimeofday(&tt1,&tz);
            test.insert(objTemp);
            gettimeofday(&tt2,&tz);

            outIa << endNumber << "," << STEP << "," <<  NumberCurrent << "," << (tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec) << "," << opCountLoad << "," << opCountSet<< endl;

            opCountLoad = 0;
            opCountSet = 0;
            gettimeofday(&tt1,&tz);
            rangeCount = test.rangeQuery(range).size();
            gettimeofday(&tt2,&tz);

            outRa << endNumber << "," << STEP << "," <<  NumberCurrent << "," << rangeCount << "," << (tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec) << "," << opCountLoad << "," << opCountSet<< endl;

            NumberCurrent ++;
        }

    }

    NumberCurrent --;
    cout << "insert ended" << endl;

    for( int i = 0; i < invCount; i++ )
    {
        if(i%10 == 0)
        {
            cout << "Delete " << NumberCurrent << endl;
        }

        for( int j = 0; j < STEP; j ++ )
        {

            opCountLoad = 0;
            opCountSet = 0;

            objTemp.id = intToString(xtoRemove[NumberCurrent]) + "P" + intToString(ytoRemove[NumberCurrent]);
            objTemp.x = xtoRemove[NumberCurrent];
            objTemp.y = ytoRemove[NumberCurrent];

            opCountLoad = 0;
            opCountSet = 0;
            gettimeofday(&tt1,&tz);
            test.remove(objTemp.id);
            gettimeofday(&tt2,&tz);

            outDa << endNumber << "," << STEP << "," <<  NumberCurrent << "," << (tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec) << "," << opCountLoad << "," << opCountSet<< endl;

            NumberCurrent --;
        }


    }

    delete xtoRemove;
    delete ytoRemove;


    cout << "all ended" << endl;

    storage.flushAll();
    outDa.close();
    outIa.close();
    outRa.close();

}

void testFromInitSuq(int endNumber, string testId) {

    struct timezone tz;
    struct timeval tt1, tt2;
    string gobo = "total";

    ofstream outIa,outDa,outRa;

    outIa.open(gobo + "_insert"+".csv",ios::app);
    outDa.open(gobo + "_delete"+".csv",ios::app);
    outRa.open(gobo + "_range"+".csv",ios::app);
    if( !outIa || !outDa || !outRa)
    {
        cout << "hehehehehehehehehehehehehehehehe" << endl;
        return;
    }


    Storage storage;
    storage.flushAll();
    TreeInterface test(storage);



    srand((unsigned)time(0));

    memset(xNumbers,0,MAX*sizeof(bool));
    memset(yNumbers,0,MAX*sizeof(bool));


    // init tree
    const int STEP = 10;

    int x=1,y=1;
    int inv = (MAX/endNumber)*STEP;
    int invCount = MAX/inv;
    assert( (MAX/endNumber)*STEP >STEP*3 );

    Range range(1,2,1,2);
    Object objTemp("z",1,1,"w");
    int xBase = inv*( rand()%invCount );
    int yBase = inv*( rand()%invCount );
    int NumberCurrent = 0;

    int* xtoRemove = new int[STEP*invCount];
    int* ytoRemove = new int[STEP*invCount];
    memset(xtoRemove,0,STEP*invCount*sizeof(int));
    memset(ytoRemove,0,STEP*invCount*sizeof(int));
    int rangeCount;

    cout << "insert and range start" << endl;

    for( int i = 0; i < invCount; i++ )
    {
        if(i%10 == 0)
        {
            cout << "Insert " << NumberCurrent << endl;
        }

        //select inv
        while( xNumbers[xBase] == true )
        {
            xBase = inv*i;
        }

        while( yNumbers[yBase] == true )
        {
            yBase = inv*i;
        }

        xNumbers[xBase] = true;
        yNumbers[yBase] = true;

        x = xBase + rand()%inv;
        y = yBase + rand()%inv;
        range.xMin = x;
        range.xMax = x + 1;
        range.yMin = y;
        range.yMax = y + 1;

        for( int j = 0; j < STEP; j ++ )
        {


            while( xNumbers[x] == true )
            {
                x = xBase + rand()%inv;
            }

            while( yNumbers[y] == true )
            {
                y = yBase + rand()%inv;
            }

            xtoRemove[NumberCurrent] = x;
            ytoRemove[NumberCurrent] = y;

            xNumbers[x] = true;
            yNumbers[y] = true;

            if( range.xMin > x )
            {
                range.xMin = x ;
            }
            if( range.yMin > y)
            {
                range.yMin = y;
            }

            if( range.xMax < x + 1 )
            {
                range.xMax = x + 1;
            }

            if( range.yMax < y + 1 )
            {
                range.yMax = y + 1;
            }

            objTemp.id = intToString(x) + "P" + intToString(y);
            objTemp.x = x;
            objTemp.y = y;

            opCountLoad = 0;
            opCountSet = 0;
            gettimeofday(&tt1,&tz);
            test.insert(objTemp);
            gettimeofday(&tt2,&tz);

            outIa << endNumber << "," << STEP << "," <<  NumberCurrent << "," << (tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec) << "," << opCountLoad << "," << opCountSet<< endl;

            opCountLoad = 0;
            opCountSet = 0;
            gettimeofday(&tt1,&tz);
            rangeCount = test.rangeQuery(range).size();
            gettimeofday(&tt2,&tz);

            outRa << endNumber << "," << STEP << "," <<  NumberCurrent << "," << rangeCount << "," << (tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec) << "," << opCountLoad << "," << opCountSet<< endl;

            NumberCurrent ++;
        }

    }

    NumberCurrent --;
    cout << "insert ended" << endl;

    for( int i = 0; i < invCount; i++ )
    {
        if(i%10 == 0)
        {
            cout << "Delete " << NumberCurrent << endl;
        }

        for( int j = 0; j < STEP; j ++ )
        {

            opCountLoad = 0;
            opCountSet = 0;

            objTemp.id = intToString(xtoRemove[NumberCurrent]) + "P" + intToString(ytoRemove[NumberCurrent]);
            objTemp.x = xtoRemove[NumberCurrent];
            objTemp.y = ytoRemove[NumberCurrent];

            opCountLoad = 0;
            opCountSet = 0;
            gettimeofday(&tt1,&tz);
            test.remove(objTemp.id);
            gettimeofday(&tt2,&tz);

            outDa << endNumber << "," << STEP << "," <<  NumberCurrent << "," << (tt2.tv_sec-tt1.tv_sec)*1000000 + (tt2.tv_usec+tt1.tv_usec) << "," << opCountLoad << "," << opCountSet<< endl;

            NumberCurrent --;
        }


    }

    delete xtoRemove;
    delete ytoRemove;


    cout << "all ended" << endl;

    storage.flushAll();
    outDa.close();
    outIa.close();
    outRa.close();

}


void initTreeSuq2(TreeInterface &anInterface, int baseNumber, int endNumber, int lastNumber, int currentNumber) {

    int x=1,y=1;
    int inv = MAX/endNumber;
    assert( MAX/endNumber >=9 );

    for( int i=0;i<(currentNumber - lastNumber);i++ )
    {
        while( xNumbers[x] == true )
        {
            x = rand()%(inv) + i*inv;
        }

        while( yNumbers[y] == true )
        {
            y = rand()%(inv) + i*inv;
        }

        xNumbers[x] = true;
        yNumbers[y] = true;

        Object obj(intToString(x) + "P" + intToString(y),x,y,"w");
        anInterface.insert(obj);
    }

}

void initTreeSuq(TreeInterface& anInterface, int baseNumber) {

    int x=1,y=1;
    int inv = MAX/baseNumber;
    assert( MAX/baseNumber >=9 );

    for( int i=0;i<baseNumber;i++ )
    {
        while( xNumbers[x] == true )
        {
            x = rand()%(inv) + i*inv;
        }

        while( yNumbers[y] == true )
        {
            y = rand()%(inv) + i*inv;
        }

        xNumbers[x] = true;
        yNumbers[y] = true;

        Object obj(intToString(x) + "P" + intToString(y),x,y,"w");
        anInterface.insert(obj);
    }

}

void initTreeRand(TreeInterface& anInterface, int baseNumber) {
    int x=1,y=1;
    for( int i=0;i<baseNumber;i++ )
    {
        while( xNumbers[x] == true )
        {
            x = rand()%MAX;
        }

        while( yNumbers[y] == true )
        {
            y = rand()%MAX;
        }

        xNumbers[x] = true;
        yNumbers[y] = true;

        Object obj(intToString(x) + "P" + intToString(y),x,y,"w");
        anInterface.insert(obj);
    }

}

void initTreeLined(TreeInterface& anInterface, int baseNumber) {

    assert( baseNumber < MAX/10 );

    int inv = MAX / (baseNumber*2);

    int x=1,y=1;
    for( int i=0;i<baseNumber;i++ )
    {
        while( xNumbers[x] == true && (x/inv)%2 == 0 )
        {
            x = rand()%MAX;
        }

        while( yNumbers[y] == true )
        {
            y = rand()%MAX;
        }

        xNumbers[x] = true;
        yNumbers[y] = true;

        Object obj(intToString(x) + "P" + intToString(y),x,y,"w");
        anInterface.insert(obj);
    }

}

void initTreeLinedVec(TreeInterface& anInterface, int baseNumber) {

    assert( baseNumber < MAX/10 );

    int inv = MAX / (baseNumber*2);

    int x=1,y=1;
    for( int i=0;i<baseNumber;i++ )
    {
        while( xNumbers[x] == true  )
        {
            x = rand()%MAX;
        }

        while( yNumbers[y] == true && (y/inv)%2 == 0 )
        {
            y = rand()%MAX;
        }

        xNumbers[x] = true;
        yNumbers[y] = true;

        Object obj(intToString(x) + "P" + intToString(y),x,y,"w");
        anInterface.insert(obj);
    }

}