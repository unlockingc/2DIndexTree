//
// Created by houyx on 2/2/17.
//
#include "Type.h"

int opCountLoad =0,opCountSet = 0;

NodeId getSubId( NodeId a )
{
    int temp = (int)(a.find(NODE_SPLITTER));
    return temp == -1 ? a: a.substr(temp + 1);
}

NodeId getBelongToId(NodeId a) {
    int temp = (int)(a.find(NODE_SPLITTER));
    return temp == -1 ? a : a.substr(0, temp);
}
