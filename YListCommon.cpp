//
// Created by houyx on 2/4/17.
//
#include "YListCommon.h"


void updateL1p2(double p2Y, YLnode &l1p2next, YLnode &l1p2pre)
{
    while( !l1p2next.isNull() && l1p2next.getY() < p2Y )
    {
        l1p2pre.id = l1p2next.id;
        l1p2next.id = l1p2next.getNextId();
    }

    if( !l1p2next.isNull() && l1p2next.getY() == p2Y )
    {
        l1p2pre.id = l1p2next.id;
    }
}

void updateL2p1(double p1Y, YLnode &l2p1next, YLnode &l2p1pre)
{
    while( !l2p1next.isNull() && l2p1next.getY() < p1Y )
    {
        l2p1pre.id = l2p1next.id;
        l2p1next.id = l2p1next.getNextId();
    }

    if( !l2p1next.isNull() && l2p1next.getY() == p1Y )
    {
        l2p1pre.id = l2p1next.id;
    }

}

// from small to big
/***
 * todo: deal with when l1.belongTo is NULL
 * //todo: delete old useless list item
 * //tod: fix root y list mark fixed
 *
 * //fix: add itself fixed
 *
 * @param l1 left
 * @param l2 right
 * @param belongTo
 * @return
 */
void merge(YList l1, YList l2, XNode &belongTo, Object obj)
{
    //YList( Storage& storage_,  XNode& belongTo_, NodeId header_, NodeId tail_, bool isLeftChild_ )
    //YList ret( belongTo.storage, belongTo, NULL_REDIS, NULL_REDIS, belongTo.isLeftChild()  );



    YLnode p1 = l1.header;
    YLnode p2 = l2.header;
    YLnode l2p1next = p2;
    YLnode l1p2next = p1;

    YLnode l2p1pre = p2;
    l2p1pre.id = NULL_REDIS;

    YLnode l1p2pre = p1;
    l1p2pre.id = NULL_REDIS;


    updateL1p2(p2.getY(), l1p2next, l1p2pre);
    updateL2p1(p1.getY(), l2p1next, l2p1pre);

    string YListMark = belongTo.storage.getUuid();
    belongTo.setYListMark(YListMark);


    YLnode pret(belongTo.storage, YListMark, NULL_REDIS );
    YLnode ptemp(belongTo.storage, YListMark, NULL_REDIS );


    if( p1.isNull() )
    {
        if( !p2.isNull() )
        {
            belongTo.setYHeader( YListMark+NODE_SPLITTER+getSubId( p2.id) );
        }
        else
        {
            belongTo.setYHeader( YListMark+NODE_SPLITTER+NULL_REDIS );
        }
    }
    else
    {
        if( p2.isNull() )
        {
            belongTo.setYHeader( YListMark+NODE_SPLITTER+getSubId( p1.id ) );
        }
        else
        {
            belongTo.setYHeader( YListMark+NODE_SPLITTER+getSubId( p1.getY() < p2.getY()? p1.id : p2.id ) );
        }

    }


    while ( !p1.isNull() && !p2.isNull() )
    {
        if( p1.getY() < p2.getY() )
        {
            pret.resetFromObject(YListMark,p1.getObject());

            if( !ptemp.isNull() )
            {
                pret.setPre(ptemp.id);
                ptemp.setNext(pret.id);
            }

            pret.setLeftLessEqualId(p1.id);
            pret.setLeftBiggerEqualId(p1.id);

            pret.setRightLessEqualId(l2p1pre.id);
            pret.setRightBiggerEqualId(l2p1next.id);


            p1.id = p1.getNextId();
            updateL2p1(p1.getY(), l2p1next, l2p1pre);

        } else
        {
            pret.resetFromObject(YListMark,p2.getObject());

            if( !ptemp.isNull() )
            {
                pret.setPre(ptemp.id);
                ptemp.setNext(pret.id);
            }

            pret.setLeftLessEqualId(l1p2pre.id);
            pret.setLeftBiggerEqualId(l1p2next.id);

            pret.setRightLessEqualId(p2.id);
            pret.setRightBiggerEqualId(p2.id);


            p2.id = p2.getNextId();
            updateL1p2(p2.getY(), l1p2next, l1p2pre);
        }

        ptemp.id = pret.id;
    }

    if( p2.isNull() && !p1.isNull() )
    {
        while ( !p1.isNull() ) {
            pret.resetFromObject(YListMark, p1.getObject());

            if (!ptemp.isNull()) {
                pret.setPre(ptemp.id);
                ptemp.setNext(pret.id);
            }

            pret.setLeftLessEqualId(p1.id);
            pret.setLeftBiggerEqualId(p1.id);

            pret.setRightLessEqualId(l2p1pre.id);
            pret.setRightBiggerEqualId(NULL_REDIS);


            p1.id = p1.getNextId();
            //updateL2p1(p1.getY(), l2p1next, l2p1pre);
            ptemp.id = pret.id;
        }
    }
    else
    {
        if( !p2.isNull() && p1.isNull() )
        {
            while ( !p2.isNull() ) {
                pret.resetFromObject(YListMark,p2.getObject());

                if( !ptemp.isNull() )
                {
                    pret.setPre(ptemp.id);
                    ptemp.setNext(pret.id);
                }

                pret.setLeftLessEqualId(l1p2pre.id);
                pret.setLeftBiggerEqualId(NULL_REDIS);

                pret.setRightLessEqualId(p2.id);
                pret.setRightBiggerEqualId(p2.id);


                p2.id = p2.getNextId();
                //updateL1p2(p2.getY(), l1p2next, l1p2pre);

                ptemp.id = pret.id;
            }
        }
    }

    belongTo.setYTail( pret.id );

    //insert  it self
    YLnode self(belongTo.storage,YListMark,obj);
    pret.resetWithFullId(belongTo.getYHeader());
    YLnode ppre(belongTo.storage, YListMark, NULL_REDIS );
    //todo: change to binary search
    while( pret.getY() < obj.y && !pret.isNull() )
    {
        ppre.id = pret.id;
        pret.resetWithFullId(pret.getNextId());
    }

    if( !ppre.isNull() )
    {
        self.setPre(ppre.id);
        ppre.setNext(self.id);
        self.setLeftLessEqualId(ppre.getLeftLessEqualId());
        self.setRightLessEqualId(ppre.getRightLessEqualId());
    }
    else
    {
        belongTo.setYHeader(self.id);
    }

    if( !pret.isNull() )
    {
        self.setNext(pret.id);
        pret.setPre(self.id);
        self.setRightBiggerEqualId(pret.getRightBiggerEqualId());
        self.setLeftBiggerEqualId(pret.getLeftBiggerEqualId());
    }
    else
    {
        belongTo.setYTail(self.id);
    }

}

/***
 * make lp toChild pointer right
 * @param lp
 * @param ll
 * @param lr
 */
void fixYList(YList &lp, YList ll, YList lr)
{
    YLnode p = lp.header;

    YLnode rbe = lr.header;
    YLnode rle = rbe;
    rle.id = NULL_REDIS;

    YLnode lbe = ll.header;
    YLnode lle = lbe;
    lle.id = NULL_REDIS;

    while( !p.isNull() )
    {
        updateL1p2(p.getY(),lbe,lle);
        updateL2p1(p.getY(),rbe,rle);

        p.setLeftLessEqualId(lle.id);
        p.setLeftBiggerEqualId(lbe.id);
        p.setRightLessEqualId(rle.id);
        p.setRightBiggerEqualId(rbe.id);

        p.id = p.getNextId();
    }

}
