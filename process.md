# note
c++11 stl to_string();
reference can not be reassign

# thoughts
1. we can directly access every node because id is the id, thus we can do something like subtree search on this
2. this is because redis allow part of the identical data become id
3. remove can do without search, which make it quicker
4. left of the node is all smaller, right of the node is all bigger
5. //search can be a small range, make it can bie ignored temporarily


# optimize
1. use map as cache to avoid redis connection time, especially in distribute system