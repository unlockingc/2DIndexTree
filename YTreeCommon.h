//
// Created by houyx on 2/4/17.
//

#ifndef INC_2DINDEXTREE_YTREECOMMON_H
#define INC_2DINDEXTREE_YTREECOMMON_H

#include "YNode.h"
#include "YLnode.h"
#include "XNode.h"

bool balancedY(YNode& g);

void updateHeightY(YNode& g);

YNode connect34Y(YNode& a,YNode& b,YNode& c,YNode t0,YNode t1, YNode t2, YNode t3);

YNode rotateAtY( YNode a );

YNode tallerChildY( YNode a );

YNode findNextY(YNode a);

YNode findPreY( YNode a );

void swapData( YNode& a, YNode& b );

#endif //INC_2DINDEXTREE_YTREECOMMON_H
