//
// Created by houyx on 1/23/17.
//

#include "YTree.h"



/*
 *    if (!v || (e == v->data)) return v; //递归基：在节点v（或假想的通配节点）处命中
   hot = v; //一般情况：先记下当前节点，然后再
   return searchIn(((e < v->data) ? v->lChild : v->rChild), e, hot); //深入一层，递归查找
 * */

YNode YTree::_searchIn( YNode& rootIn ,YNode& newNode, YNode& hot) {

    if( rootIn.isNull() || rootIn.getY()== newNode.getY() )//todo : must estimate x, otherwise insert will be wrong
    {
        return rootIn;
    }

    hot.id = rootIn.id;
    (rootIn.getY() > newNode.getY())? rootIn.id = rootIn.getValue(v(leftChild)) : rootIn.id = rootIn.getValue(v(rightChild)); //this make map useless
    return _searchIn( rootIn,newNode,hot);
}


YNode YTree::_search(YNode& newNode) {
    YNode temp(root);
    return _searchIn(temp, newNode, _hot);
}


/*
 *    BinNodePosi(T) & x = search(e); if (x) return x; //确认目标节点不存在（留意对_hot的设置）
   x = new BinNode<T>(e, _hot); _size++; //创建节点x（此后，其父_hot可能增高，祖父可能失衡）
   for (BinNodePosi(T) g = _hot; g; g = g->parent) { //从x之父出发向上，逐层检查各代祖先g
      if (!AvlBalanced(*g)) { //一旦发现g失衡，则（采用“3 + 4”算法）使之复衡
         FromParentTo(*g) = rotateAt(tallerChild(tallerChild(g))); //将该子树联至原父亲
         break; //g复衡后，局部子树高度必然复原；其祖先亦必如此，故调整随即结束
      } else //否则（g依然平衡），只需简单地
         updateHeight(g); //更新其高度（注意：即便g未失衡，高度亦可能增加）
   } //至多只需一次调整；若果真做过调整，则全树高度必然复原
   return x; //返回新节点
 * */

/*#define FromParentTo(x) ( \
   IsRoot(x) ? _root : ( \
   IsLChild(x) ? (x).parent->lChild : (x).parent->rChild \
   ) \
)*/


/***
 * todo: fix next pre pointer
 * @param object
 * @return
 */
Neighbor YTree::insert(Object &object) {
    YNode newNode( storage,object );
    YNode x = _search(newNode);

    if( !x.isNull() )
    {
/*        Neighbor( NodeId next_, NodeId pre_, NodeId belongTo_  ):next(next_),pre(pre_),yListSuff(belongTo_)*/
        return Neighbor( x.getNextId(),x.getPreId(),v(yRoot));
    }

    newNode.setValue(v(parent),_hot.id);
    newNode.setValue(v(height),"1");

    _hot.setValue( (_hot.getY() > object.y )?v(leftChild):v(rightChild), newNode.id );


    YNode next = findNextY( newNode );

        //get pointer's of a right

    if( !next.isNull() )
    {
        next.setPre(newNode.id);
        newNode.setNext( next.id);
        //todo: simplify
        newNode.setRightBiggerEqualId(next.getRightBiggerEqualId());
        newNode.setLeftBiggerEqualId(next.getLeftBiggerEqualId());
    } else
    {
        belongTo.setYTail(newNode.id);
    }

    YNode pre = findPreY( newNode);
    if( !pre.isNull() )
    {
        pre.setNext( newNode.id);
        newNode.setPre(pre.id);
        newNode.setLeftLessEqualId(pre.getLeftLessEqualId());
        newNode.setRightLessEqualId(pre.getRightLessEqualId());
    } else
    {
        belongTo.setYHeader(newNode.id);
    }




    YNode parentTemp(root);
    //adjust YTree
    for(  YNode g(_hot); !g.isNull(); g.id = g.getValue(v(parent)) )
    {
        if( !balancedY(g) )
        {
            parentTemp.id = g.getValue(v(parent));

            YNode temp = rotateAtY( tallerChildY( tallerChildY(g)));

            if( !parentTemp.isNull() )
            {
                parentTemp.setValue(( parentTemp.getY() >  g.getY() )?v(leftChild):v(rightChild), temp.id);
            }
            else
            {
                //update root
                root = temp;
            }

            break;
        }
        else
        {
            updateHeightY(g);
        }
    }

    //insert in yList from bottom to top, return per and next as a reference to insert in parent's yList

    //insert in ytree

    return Neighbor( next.id,pre.id,v(yRoot));
}


/*template <typename T> bool AVL<T>::remove(const T & e) { //从AVL树中删除关键码e
    BinNodePosi(T) & x = search(e); if (!x) return false; //确认目标节点存在（留意对_hot的设置）
    removeAt(x, _hot); _size--; //先按BST规则删除之（此后，原节点之父_hot及其祖先均可能失衡）
    for (BinNodePosi(T) g = _hot; g; g = g->parent) { //从_hot出发向上，逐层检查各代祖先g
        if (!AvlBalanced(*g)) //一旦发现g失衡，则（采用“3 + 4”算法）使之复衡
            g = FromParentTo(*g) = rotateAt(tallerChild(tallerChild(g))); //将该子树联至原父亲
        updateHeight(g); //并更新其高度（注意：即便g未失衡，高度亦可能降低）
    } //可能需做Omega(logn)次调整――无论是否做过调整，全树高度均可能降低
    return true; //删除成功
} //若目标节点存在且被删除，返回true；否则返回false*/
/***
 * todo: be careful for root change
 * @param id
 * @return
 */

Neighbor YTree::remove(NodeId &id) {
    YNode newNode( storage,id );
    YNode x = _search(newNode);

    assert(!x.isNull());//debug
    //XTree remove in ylist first

    Neighbor ret = removeAtY( x, _hot );


    YNode parentTemp(root);
    //adjust YTree
    for(  YNode g(_hot); !g.isNull(); g.id = g.getValue(v(parent)) )
    {
        if( !balancedY(g) )
        {
            parentTemp.id = g.getValue(v(parent));

            YNode temp = rotateAtY( tallerChildY( tallerChildY(g)));

            if( !parentTemp.isNull() )
            {
                parentTemp.setValue(( parentTemp.getY() >  g.getY() )?v(leftChild):v(rightChild), temp.id);
            }
            else
            {
                //update root
                root = temp;
            }

            g = temp;
        }
        updateHeightY(g);
    }

    //insert in yList from bottom to top, return per and next as a reference to insert in parent's yList

    //insert in ytree

    return ret;
}


/******************************************************************************************
 * BST节点删除算法：删除位置x所指的节点（全局静态模板函数，适用于AVL、Splay、RedBlack等各种BST）
 * x由此前的查找确定，并经确认非NULL后方调用本函数，故必删除成功
 * 与searchIn不同，调用之前不必将hot置空
 * 返回值指向实际被删除节点的接替者，hot指向实际被删除节点的父亲――二者均有可能是NULL
 ******************************************************************************************/
/*
template <typename T> static BinNodePosi(T) removeAt(BinNodePosi(T)& x, BinNodePosi(T)& hot) {
BinNodePosi(T) w = x; //实际被摘除的节点，初值同x
BinNodePosi(T) succ = NULL; //实际被删除节点的接替者
if (!HasLChild(*x)) //若*x的左子树为空，则可
succ = x = x->rChild; //直接将*x替换为其右子树
else if (!HasRChild(*x)) //若右子树为空，则可
succ = x = x->lChild; //对称地处理――注意：此时succ != NULL
else { //若左右子树均存在，则选择x的直接后继作为实际被摘除节点，为此需要
w = w->succ(); //（在右子树中）找到*x的直接后继*w
swap(x->data, w->data); //交换*x和*w的数据元素
BinNodePosi(T) u = w->parent;
((u == x) ? u->rChild : u->lChild) = succ = w->rChild; //隔离节点*w
}
hot = w->parent; //记录实际被删除节点的父亲
if (succ) succ->parent = hot; //并将被删除节点的接替者与hot相联
release(w->data); release(w); return succ; //释放被摘除节点，返回接替者
}
*/


/***
 * todo:对XTree来说，所要做的不过是吧w到x之间的原w节点删掉而已（x节点在之前已经全部清除了）
 * todo: swap can be optimized
 * @param x
 * @param hot
 * @return
 */
Neighbor YTree::removeAtY(YNode &x, YNode &hot) {

    bool isXRoot = (root.id == x.id);

    YNode w = x;
    YNode succ = w;
    bool isWLeftchild;

    if(!x.hasLeftChild())
    {
        succ = x.getRightChild();


        //never can be null because the initRoot
        if(isXRoot)
        {
            root = succ;
        }

        hot = w.getParent();
        isWLeftchild = w.isLeftChild();
    }
    else if(!x.hasRightChild())
    {
        succ = x.getLeftChild();

        if(isXRoot)
        {
            root = succ;
        }

        hot = w.getParent();
        isWLeftchild = w.isLeftChild();
    }
    else
    {
        w = findNextY(w);

        if(isXRoot)
        {
            root = w;
        }


        succ = w.getRightChild();
        isWLeftchild = w.isLeftChild();

        swapData( x, w );



        w = x;

        hot = w.getParent();

        //(u == x) ? u.setRightChild(succ):u.setLeftChild(succ);//wrong!!!!fix
    }


    YLnode pre = w.getPre();
    YLnode next = w.getNext();


    //即使为空也必须设置
    if( pre.isNull() )
    {
        belongTo.setYHeader(next.id);
    }
    else
    {
        pre.setNext(next.id);
    }

    if( next.isNull() )
    {
        belongTo.setYTail(pre.id);
    } else
    {
        next.setPre(pre.id);
    }



    isWLeftchild?hot.setLeftChild(succ):hot.setRightChild(succ);
    if(!succ.isNull())
    {
        succ.setParent(hot);
    }
    w.deleteSelf();
    return Neighbor( next.id,pre.id,v(yRoot));

}

YLnode YTree::getYLess(Object &min, Object &max) {


    YNode temp(root);
    YNode minSearch(storage,min);

    YNode found = _searchIn(temp,minSearch,_hot);

    if( !found.isNull() && found.getY() == min.y )
    {
        return found;
    } else
    {
        if( _hot.getY() > min.y )
        {
            return _hot;
        }
        else
        {
            return _hot.getNext();
        }
    }

}

YLnode YTree::getYBigger(Object &range, Object &max) {

    YNode temp(root);
    YNode maxSearch(storage,max);

    YNode found = _searchIn(temp,maxSearch,_hot);


    if( !found.isNull() && found.getY() == max.y  )
    {
        return found;
    } else
    {
        if( _hot.getY() < max.y )
        {
            return _hot;
        }
        else
        {
            return _hot.getPre();
        }
    }

}
