//
// Created by houyx on 1/24/17.
//

#include "YList.h"


/***
 * fix x parent neighbor and return self neighbor:
 *      1. let child use it to get self neighbor
 *      2. let child use it to fix x parent neighbor
 * @param object
 * @param neighbor
 * @return
 */
Neighbor YList::insert(Object& object, Neighbor& neighbor) {
    YLnode parentPre(   storage, neighbor.pre, 1);
    YLnode parentNext(  storage, neighbor.next, 1);

    //depend on if return of get...Id() contains yListSuff field, current is contains
    YLnode pre( storage, isLeftChild?parentPre.getLeftLessEqualId():parentPre.getRightLessEqualId(),1 );
    YLnode next( storage,isLeftChild?parentNext.getLeftBiggerEqualId():parentNext.getRightBiggerEqualId(),1 );

    //insert
    assert( pre.isNull() || pre.getNext().id == next.id );
    assert( next.isNull() || next.getPre().id == pre.id );


    YLnode a(storage,belongTo.getYListMark(),object);

    //get pointer's of a right


    if( !next.isNull() )
    {
        a.setNext(next.id);
        next.setPre(a.id);
        //todo: simplify
        a.setRightBiggerEqualId(next.getRightBiggerEqualId());
        a.setLeftBiggerEqualId(next.getLeftBiggerEqualId());

    }
    else
    {
        belongTo.setYTail( a.id );
    }

    if( !pre.isNull() )
    {
        a.setPre(pre.id);
        pre.setNext(a.id);
        a.setLeftLessEqualId(pre.getLeftLessEqualId());
        a.setRightLessEqualId(pre.getRightLessEqualId());
    }
    else
    {
        belongTo.setYHeader(a.id);
    }
    //todo: check if keep other field null is right

    //fixparent
    NodeId myId = neighbor.yListSuff + NODE_SPLITTER + object.id;

    isLeftChild?fixParentByLeft( (pre.isNull()?NULL_REDIS:(neighbor.yListSuff + NODE_SPLITTER + getSubId(pre.id))),(next.isNull()?NULL_REDIS:(neighbor.yListSuff +NODE_SPLITTER+getSubId(next.id))), myId,a.id):fixParentByRight( (pre.isNull()?NULL_REDIS:(neighbor.yListSuff+ NODE_SPLITTER+getSubId(pre.id))),(next.isNull()?NULL_REDIS:(neighbor.yListSuff +NODE_SPLITTER+getSubId(next.id))), myId,a.id);


    Neighbor ret( next.id, pre.id, belongTo.getYListMark() );

    return ret;
}


/***
 * from middle to border, avoid NULL fault
 * @param preInParent
 * @param nextInParent
 * @param myId
 * @param target
 */
void YList::fixParentByLeft(NodeId preInParent, NodeId nextInParent, NodeId &myId, NodeId &target)
{
    YLnode it(storage,myId,1);
    //lessequal
    while (!it.isNull()&&it.id!=nextInParent )
    {
        it.setLeftLessEqualId(target);
        it.id = it.getNextId();
    }

/*    if( it.id == nextInParent && !it.isNull() )
    {
        it.setLeftLessEqualId(target);
    }*/

    it.id = myId;


    //biggerEqual
    while (!it.isNull()&&it.id!=preInParent )
    {
        it.setLeftBiggerEqualId(target);
        it.id = it.getPreId();
    }

/*    if( it.id == preInParent && !it.isNull() )
    {
        it.setLeftLessEqualId(target);
    }*/
}

void YList::fixParentByRight(NodeId preInParent, NodeId nextInParent, NodeId &myId, NodeId &target)
{
    YLnode it(storage,myId,1);
    //lessequal
    while (!it.isNull()&&it.id!=nextInParent )
    {
        it.setRightLessEqualId(target);
        it.id = it.getNextId();
    }

/*    if( it.id == nextInParent && !it.isNull() )
    {
        it.setRightLessEqualId(target);
    }*/

    it.id = myId;


    //biggerEqual
    while (!it.isNull()&&it.id!=preInParent )
    {
        it.setRightBiggerEqualId(target);
        it.id = it.getPreId();
    }

/*    if( it.id == preInParent && !it.isNull() )
    {
        it.setRightLessEqualId(target);
    }*/
}

void YList::resetByXNode( XNode& a )
{
    belongTo = a;
    header.belongTo = a.getYListMark();
    header.id = a.getYHeader();

    tail.belongTo = header.belongTo;
    tail.id = a.getYTail();
    isLeftChild = a.isLeftChild();
}


Neighbor YList::remove(Object &object, Neighbor &neighbor) {
    YLnode parentPre(   storage, neighbor.pre, 1);
    YLnode parentNext(  storage, neighbor.next, 1);

    //depend on if return of get...Id() contains yListSuff field, current is contains
    YLnode pre( storage, isLeftChild?parentPre.getLeftLessEqualId():parentPre.getRightLessEqualId(),1 );
    YLnode next( storage,isLeftChild?parentNext.getLeftBiggerEqualId():parentNext.getRightBiggerEqualId(),1 );


    //debug
    if( pre.getNext().id != belongTo.getYListMark()+NODE_SPLITTER+object.id )
    {
        NodeId temp =  pre.getNextId();
        string temp1 = belongTo.getYListMark()+NODE_SPLITTER+object.id;
        bool test = (temp1 == temp);

    }

    if(  next.getPre().id != belongTo.getYListMark()+NODE_SPLITTER+object.id )
    {
        string temp = next.getPreId();
        string temp1 = belongTo.getYListMark()+NODE_SPLITTER+object.id;
        bool test = (temp1 == temp);

    }

    assert( pre.getNext().id == belongTo.getYListMark()+NODE_SPLITTER+object.id || pre.isNull() );
    assert( next.getPre().id == belongTo.getYListMark()+NODE_SPLITTER+object.id || next.isNull() );

    YLnode a(storage,belongTo.getYListMark(),object.id);

    if( !next.isNull() )
    {
        next.setPre(pre.id);
    }
    else
    {
        belongTo.setYTail( pre.id );
    }

    if( !pre.isNull() )
    {
        pre.setNext(next.id);
    }
    else
    {
        belongTo.setYHeader(next.id);
    }
    //todo: check if keep other field null is right

    YLnode me(storage,belongTo.getYListMark(),object.id);
    me.deleteSelf();

    isLeftChild ? removeFixParentByLeft(pre.id, next.id, parentPre, parentNext, (pre.isNull() ? NULL_REDIS : (neighbor.yListSuff + NODE_SPLITTER + getSubId(pre.id))), (next.isNull() ? NULL_REDIS : (neighbor.yListSuff + NODE_SPLITTER + getSubId(next.id))))
                : removeFixParentByRight(pre.id, next.id, parentPre, parentNext, (pre.isNull() ? NULL_REDIS : (neighbor.yListSuff + NODE_SPLITTER + getSubId(pre.id))), (next.isNull() ? NULL_REDIS : (neighbor.yListSuff + NODE_SPLITTER + getSubId(next.id))));


    Neighbor ret( next.id, pre.id, belongTo.getYListMark() );

    return ret;
}

void YList::removeFixParentByLeft(NodeId preId, NodeId nextId, YLnode &parentPre, YLnode &parentNext, NodeId preInParent,
                                  NodeId nextInParent) {

    //lessequal
    while (!parentNext.isNull()&&parentNext.id!=nextInParent )
    {
        parentNext.setLeftLessEqualId(preId);
        parentNext.id = parentNext.getNextId();
    }



    //biggerEqual
    while (!parentPre.isNull()&&parentPre.id!=preInParent )
    {
        parentPre.setLeftBiggerEqualId(nextId);
        parentPre.id = parentPre.getPreId();
    }



}

void YList::removeFixParentByRight(NodeId preId, NodeId nextId, YLnode &parentPre, YLnode &parentNext, NodeId preInParent,
                                   NodeId nextInParent) {


    //lessequal
    while (!parentNext.isNull()&&parentNext.id!=nextInParent )
    {
        parentNext.setRightLessEqualId(preId);
        parentNext.id = parentNext.getNextId();
    }



    //biggerEqual
    while (!parentPre.isNull()&&parentPre.id!=preInParent )
    {
        parentPre.setRightBiggerEqualId(nextId);
        parentPre.id = parentPre.getPreId();
    }
}

