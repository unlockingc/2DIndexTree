//
// Created by houyx on 1/31/17.
//

#include "YNode.h"

bool YNode::isRoot() {
    return getParent().isNull();
}

bool YNode::operator==(const YNode &c4)
{
    return id==c4.id;
}

bool YNode::setLeftChild( YNode& a )
{
    return setValue(v(leftChild),a.id);
}

bool YNode::setRightChild( YNode& a )
{
    return setValue(v(rightChild),a.id);
}

bool YNode::setParent( YNode& a )
{
    return setValue(v(parent),a.id);
}

bool YNode::setHeight(int h)
{
    return setValue(v(height), to_string(h) );
}

int YNode::getHeight() {
    string temp = getValue(v(height));
    return temp == "NULL"? 0: atoi(temp.c_str());
}

YNode YNode::getRightChild() {

    return YNode(storage,getValue(v(rightChild)),1);
}

YNode YNode::getLeftChild() {
    return YNode(storage,getValue(v(leftChild)),1);
}

YNode YNode::getParent() {
    return YNode(storage,getValue(v(parent)),1);
}

bool YNode::isLeftChild() {
    if( isRoot() )
    {
        return false;
    }
    else
    {
        return (getParent().getLeftChild() == *this);
    }
}
bool YNode::isRightChild() {
    if( isRoot() )
    {
        return false;
    }
    else
    {
        return (getParent().getRightChild() == *this);
    }
}

bool YNode::hasLeftChild() {
    return getValue(v(leftChild)) != NULL_REDIS;
}

bool YNode::hasRightChild() {
    return getValue(v(rightChild)) != NULL_REDIS;
}

bool YNode::hasParent() {
    return getValue(v(parent)) != NULL_REDIS;
}


