//
// Created by houyx on 1/22/17.
//



#pragma once
#ifndef INC_2DINDEXTREE_TESTCLASS_H
#define INC_2DINDEXTREE_TESTCLASS_H


#include <stdlib.h>
#include <algorithm>
#include <iostream>
#include <map>
#include <vector>
#include <cstring>
#include <assert.h>
#include <hiredis/hiredis.h>

using namespace std;

class TestClass{
public:
    int temp;


    TestClass( int temp_ ) : temp(temp_)
    {}

    bool operator==(const TestClass &c4)
    {
        return temp == c4.temp;
    }

    bool isSameasMe( TestClass& a )
    {
        return a == *this;
    }

    void test( string a )
    {
        cout << a << endl;
    };

};

#endif //INC_2DINDEXTREE_TESTCLASS_H
