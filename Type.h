//
// Created by houyx on 1/23/17.
//
#pragma once
#ifndef INC_2DINDEXTREE_TYPE_H
#define INC_2DINDEXTREE_TYPE_H

#include <map>
#include <assert.h>

#define v(x) #x
#define NULL_REDIS "NULL"
#define NODE_SPLITTER ":"

using namespace std;

typedef     map<string,   string>   ssmap;

typedef     string  NodeId;

//useless, just for auto coding hit
static int x,y,content,id,leftChild,rightChild,parent,yRoot,height,yTail,yHeader,yListMark;
extern int opCountLoad,opCountSet;


class Object
{
public:
    NodeId id;//identify
    double x;
    double y;
    string content;

    Object():x(-1),y(-1),content("w"),id("zz")
    {

    }

    Object(NodeId id_,double x_, double y_, string content_):x(x_),y(y_),content(content_),id(id_)
    {
    }
};

class Range
{
    public:
    double xMin,xMax,yMin,yMax;
    Range(double xMin_, double xMax_, double yMin_, double yMax_ ) : xMin(xMin_), xMax(xMax_), yMin(yMin_),yMax(yMax_)
    {
        assert( xMin_ <= xMax_ );
        assert( yMin_ <= yMax_ );
    }
};

class Neighbor
{
public:
    NodeId next;
    NodeId pre;
    NodeId yListSuff;
    Neighbor( NodeId next_, NodeId pre_, NodeId yListSuff_  ):next(next_),pre(pre_),yListSuff(yListSuff_)
    {

    }
};


NodeId getSubId( NodeId a );

NodeId getBelongToId( NodeId a );


#endif //INC_2DINDEXTREE_TYPE_H
