//
// Created by houyx on 1/25/17.
//
#include "TreeCommon.h"



/*#define BalFac(x) (stature((x).lChild) - stature((x).rChild)) //平衡因子
#define AvlBalanced(x) ((-2 < BalFac(x)) && (BalFac(x) < 2)) //AVL平衡条件*/


bool balanced(XNode& g)
{
    if(g.isNull())
    {
        return true;
    }

    int temp = g.getLeftChild().getHeight() - g.getRightChild().getHeight();
    return temp > -2 && temp < 2;
}

void updateHeight(XNode& g)
{
    if(g.isNull())
    {
        g.setHeight(0);
        return;
    }
    int tempLeft = g.getLeftChild().getHeight();
    int tempRight = g.getRightChild().getHeight();

    g.setHeight( tempLeft > tempRight? tempLeft + 1 : tempRight + 1 );
}


/**
 * todo: consider relative y tree reconstruct
 * @param a
 * @param b
 * @param c
 * @param t0
 * @param t1
 * @param t2
 * @param t3
 * @return
 */
XNode connect34(XNode &a, XNode &b, XNode &c, XNode t0, XNode t1, XNode t2, XNode t3, YList gYList)
{
    b.setYListMark(gYList.belongTo.getYListMark());
    gYList.belongTo.id = b.id;
    b.setYHeader(gYList.header.id);
    b.setYTail(gYList.tail.id);
    gYList.header.belongTo = b.id;
    gYList.tail.belongTo = b.id;



    a.setLeftChild(t0);
    if(!t0.isNull())
    {
        t0.setParent(a);
    }
    a.setRightChild(t1);
    if(!t1.isNull())
    {
        t1.setParent(a);
    }

    YNode aa(a.storage,a.id);
    merge(YList(a.storage, t0, t0.getYHeader(), t0.getYTail(), t0.isLeftChild()),
          YList(a.storage, t1, t1.getYHeader(), t1.getYTail(), t1.isLeftChild()), a, aa.getObject() );
    updateHeight(a);

    c.setLeftChild(t2);
    if( !t2.isNull() )
    {
        t2.setParent(c);
    }
    c.setRightChild(t3);
    if( !t3.isNull() )
    {
        t3.setParent(c);
    }

    YNode cc(c.storage,c.id);
    merge(YList(a.storage, t2, t2.getYHeader(), t2.getYTail(), t2.isLeftChild()),
          YList(a.storage, t3, t3.getYHeader(), t3.getYTail(), t3.isLeftChild()), c, cc.getObject());
    updateHeight(c);

    b.setLeftChild(a);
    a.setParent(b);
    b.setRightChild(c);
    c.setParent(b);

    fixYList(gYList, YList(a.storage, a, a.getYHeader(), a.getYTail(), a.isLeftChild()),
             YList(c.storage, c, c.getYHeader(), c.getYTail(), c.isLeftChild()));
    updateHeight(b);


    return b;
}

XNode rotateAt( XNode v )
{
    assert(!v.isNull());

    XNode p = v.getParent(); XNode g = p.getParent();
    XNode temp = g.getParent();

    if( p.isLeftChild() )
    {
        if( v.isLeftChild() )
        {
            p.setParent( temp );

            return connect34(v, p, g,
                             v.getLeftChild(),
                             v.getRightChild(),
                             p.getRightChild(),
                             g.getRightChild(), YList(g.storage,g,g.getYHeader(),g.getYTail(),g.isLeftChild()));
        }
        else
        {
            v.setParent( temp );
            return connect34(p, v, g,
                             p.getLeftChild(),
                             v.getLeftChild(),
                             v.getRightChild(),
                             g.getRightChild(), YList(g.storage,g,g.getYHeader(),g.getYTail(),g.isLeftChild()));
        }
    }
    else
    {
        if( v.isRightChild() )
        {
            p.setParent( temp );
            return connect34(g, p, v,
                             g.getLeftChild(),
                             p.getLeftChild(),
                             v.getLeftChild(),
                             v.getRightChild(), YList(g.storage,g,g.getYHeader(),g.getYTail(),g.isLeftChild()));
        }
        else
        {
            v.setParent( temp );
            return connect34(g, v, p,
                             g.getLeftChild(),
                             v.getLeftChild(),
                             v.getRightChild(),
                             p.getRightChild(), YList(g.storage,g,g.getYHeader(),g.getYTail(),g.isLeftChild()));
        }
    }
}


XNode tallerChild( XNode a )
{
    XNode leftChild = a.getLeftChild();
    XNode rightChild = a.getRightChild();


    if( leftChild.getHeight() > rightChild.getHeight() )
    {
        return leftChild;
    }
    else
    {
        if( leftChild.getHeight() < rightChild.getHeight() )
        {
            return rightChild;
        }
        else
        {
            if( a.isLeftChild() )//return the same side
            {
                return leftChild;
            }
            else
            {
                return rightChild;
            }
        }
    }
}

/***
 * 这时候由于w已经被删除，所以w的列表项是和succ完全相同的
 * balance tree succ list no more than 1 items
 * after this, original succ YList would be null and useless
 * @param w
 * @param succ
 */
void fixListPointer(XNode &w, XNode &succ) {
    if(succ.isNull())
    {
        return;
    }

    YList wList(w.storage,w,w.getYHeader(),w.getYTail(),w.isLeftChild());
    YList sList(succ.storage,succ,succ.getYHeader(),succ.getYTail(),succ.isLeftChild());

    YLnode wIt = wList.header;
    YLnode sIt = sList.header;
    YLnode temp = sIt;

    if( wIt.isNull() )
    {
        return;
    }

    while( !wIt.isNull() )
    {
        assert(!sIt.isNull());
        wIt.setLeftBiggerEqualId(sIt.getLeftBiggerEqualId());
        wIt.setLeftLessEqualId(sIt.getLeftLessEqualId());
        wIt.setRightBiggerEqualId(sIt.getRightBiggerEqualId());
        wIt.setRightLessEqualId(sIt.getRightLessEqualId());

        wIt = wIt.getNext();

        temp = sIt;
        sIt = sIt.getNext();
        temp.deleteSelf();
    }

    succ.setYListMark(w.getYListMark());
    succ.setYHeader(w.getYHeader());
    succ.setYTail(w.getYTail());

}


/***
 *
 * @param a is x
 * @param b is w
 */
void swapDataX(XNode &a, XNode &b) {
    if( a.getParent() == b )
    {
        bool bisLeftChild = b.isLeftChild(), aisLeftChild = a.isLeftChild();
        XNode temp = b.getParent(), bOr = aisLeftChild? b.getRightChild():b.getLeftChild(), aL = a.getLeftChild(), aR = a.getRightChild();

        a.setParent(temp);
        if(!temp.isNull())
        {
            bisLeftChild? temp.setLeftChild(a):temp.setRightChild(a);
        }

        aisLeftChild? a.setRightChild(bOr):a.setLeftChild(bOr);
        if(!bOr.isNull()) {
            bOr.setParent(a);
        }

        aisLeftChild? a.setLeftChild(b):a.setRightChild(b);
        b.setParent(a);

        b.setLeftChild(aL);
        if(!aL.isNull())
        {
            aL.setParent(b);
        }

        b.setRightChild(aR);
        if(!aR.isNull())
        {
            aR.setParent(b);
        }
    }
    else if( b.getParent() == a )
    {
        bool bisLeftChild = b.isLeftChild(), aisLeftChild = a.isLeftChild();
        XNode temp = a.getParent(), aOr = bisLeftChild? a.getRightChild():a.getLeftChild(), bL = b.getLeftChild(), bR = b.getRightChild();

        b.setParent(temp);
        if(!temp.isNull())
        {
            aisLeftChild? temp.setLeftChild(b):temp.setRightChild(b);
        }

        bisLeftChild? b.setRightChild(aOr):b.setLeftChild(aOr);
        if(!aOr.isNull()) {
            aOr.setParent(b);
        }

        bisLeftChild? b.setLeftChild(a):b.setRightChild(a);
        a.setParent(b);

        a.setLeftChild(bL);
        if(!bL.isNull())
        {
            bL.setParent(a);
        }

        a.setRightChild(bR);
        if(!bR.isNull())
        {
            bR.setParent(a);
        }
    }
    else
    {

        bool bisLeftChild = b.isLeftChild(), aisLeftChild = a.isLeftChild();
        XNode tempa = a.getParent(),tempb = b.getParent(), aL = a.getLeftChild(), aR = a.getRightChild(), bL = b.getLeftChild(), bR = b.getRightChild();

        a.setParent(tempb);
        if(!tempb.isNull())
        {
            bisLeftChild? tempb.setLeftChild(a):tempb.setRightChild(a);
        }

        b.setParent(tempa);
        if(!tempa.isNull())
        {
            aisLeftChild? tempa.setLeftChild(b):tempa.setRightChild(b);
        }

        a.setLeftChild(bL);
        if(!bL.isNull())
        {
            bL.setParent(a);
        }

        a.setRightChild(bR);
        if(!bR.isNull())
        {
            bR.setParent(a);
        }

        b.setLeftChild(aL);
        if(!aL.isNull())
        {
            aL.setParent(b);
        }

        b.setRightChild(aR);
        if(!aR.isNull())
        {
            aR.setParent(b);
        }
    }

    NodeId temp = b.getYListMark();
    b.setYListMark(a.getYListMark());
    a.setYListMark(temp);

    temp = b.getYHeader();
    b.setYHeader(a.getYHeader());
    a.setYHeader(temp);

    temp = b.getYTail();
    b.setYTail(a.getYTail());
    a.setYTail(temp);

    int temp1 = b.getHeight();
    b.setHeight(a.getHeight());
    a.setHeight(temp1);

}


/*
 *     XNode ret = a;
    XNode s = a.getRightChild();
    if( !s.isNull() )
    {
        while( !s.isNull() )
        {
            ret.id = s.id;
            s = s.getLeftChild();
        }
    }
    else
    {
        while( ret.isRightChild() )
        {
            ret = ret.getParent();
        }
        ret = ret.getParent();
    }
    return  ret;
}
 * */

/*XNode ret = a;
XNode s = a.getRightChild();
if( !s.isNull() )
{
while( !s.isNull() )
{
ret.id = s.id;
s = s.getLeftChild();
}
}*/

void deleteWFromWtoX(XNode &x, XNode &w) {

    Object temp(w.id,w.getX(),2,"hehe");

    double xx = x.getX();
    YLnode winW(w.storage,w.getYListMark(),w.id);
    Neighbor neighbor(winW.getNextId(),winW.getPreId(),w.getYListMark());

    //todo: optmize
    if( !winW.getPre().isNull() ) {
         winW.setRightLessEqualId( winW.getPre().getRightLessEqualId() );
    } else{
        winW.setRightLessEqualId(NULL_REDIS);
    }

    if(!winW.getNext().isNull())
    {
         winW.setRightBiggerEqualId(winW.getNext().getRightBiggerEqualId());
    } else
    {
         winW.setRightBiggerEqualId(NULL_REDIS);
    }

    w.id = w.getValue(v(rightChild));

    YList wList(w.storage,w,w.getYHeader(),w.getYTail(),w.isLeftChild());

    while( ! (x == w) )
    {
        neighbor = wList.remove(temp,neighbor);
        w.id = w.getValue(v(leftChild));
        wList.resetByXNode(w);
    }

    assert(w==x);
    wList.remove(temp,neighbor);

}


/***
 * can be NULL means it don't have pre
 * @param a
 * @return
 */
XNode findPre(XNode a)
{
    XNode ret = a;
    XNode s = a.getLeftChild();
    if( !s.isNull() )
    {
        while( !s.isNull() )
        {
            ret.id = s.id;
            s = s.getRightChild();
        }
    }
    else
    {
        while( ret.isLeftChild() )
        {
            ret = ret.getParent();
        }
        ret = ret.getParent();
    }
    return  ret;
}

/***
 * can be NULL means it don't have next
 * @param a
 * @return
 */
XNode findNext(XNode a)
{
    XNode ret = a;
    XNode s = a.getRightChild();
    if( !s.isNull() )
    {
        while( !s.isNull() )
        {
            ret.id = s.id;
            s = s.getLeftChild();
        }
    }
    else
    {
        while( ret.isRightChild() )
        {
            ret = ret.getParent();
        }
        ret = ret.getParent();
    }
    return  ret;
}
