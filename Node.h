//
// Created by houyx on 1/31/17.
//
#pragma once
#ifndef INC_2DINDEXTREE_NODE_H
#define INC_2DINDEXTREE_NODE_H

#include "Storage.h"


class Storage;


class Node
{

public:
    Storage& storage;
public:
    string id;

    //todo: can delete values because is uesless
    //ssmap values;

    Node( Storage& storage_, string key_);
    Node(const Node&) = default;
    bool operator==(const Node &c4);

    string getValue( string field );

    bool setValue( string field, string value );

    //todo: can delete values because is uesless
    void loadValue(string subKey, string value);

    void removeValue( string subKey );

    bool isNull();

    string toString();

    bool deleteSelf();
};


#endif //INC_2DINDEXTREE_NODE_H
