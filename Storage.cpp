//
// Created by houyx on 1/31/17.
//

#include "Storage.h"

using namespace std;


bool Storage::updateFieldOfNode( string key, string field, string value )
{
    /*    HMSET id field1 value1 [field2 value2 ]*/
    string command = "HSET " + key + " " + field + " " + value + " ";
    reply = (redisReply *) redisCommand(c,command.c_str());

    bool ret = (reply->type != REDIS_REPLY_ERROR);
    freeReplyObject(reply);

    return ret;
}

bool Storage::deleteFieldOfNode(string key, string field)
{
    /*    HDEL id field1 value1 [field2 value2 ]*/
    string command = "HDEL " + key + " " + field + " ";
    reply = (redisReply *) redisCommand(c,command.c_str());

    bool ret = (reply->type != REDIS_REPLY_ERROR);
    freeReplyObject(reply);

    return ret;

}


//NULL is dealt here
string Storage::getFieldOfNode( string key, string field )
{
    /*    HMSET id field1 value1 [field2 value2 ]*/
    string command = "HGET " + key + " " + field + " ";
    reply = (redisReply *) redisCommand(c,command.c_str());

    string ret = reply->str == NULL? "NULL":reply->str;
    freeReplyObject(reply);
    return ret;
}

/*bool Storage::saveNode( Node node )
{
    *//*    HMSET id field1 value1 [field2 value2 ]*//*
    string command = "HMSET " + node.toString();
    reply = (redisReply *) redisCommand(c,command.c_str());
    bool ret = (reply->type != REDIS_REPLY_ERROR);
    freeReplyObject(reply);

    return ret;
}*/

/*Node Storage::loadNode( string key )
{
    string command = "HGETALL " + key;
    reply = (redisReply *) redisCommand(c,command.c_str());
    Node ret(*this,key);

    //debug
    assert(reply->elements%2 == 0);

    for( int i = 0; i < reply->elements; i+=2 )
    {
        ret.loadValue(reply->element[i]->str, reply->element[i + 1]->str);
    }

    freeReplyObject(reply);
    return ret;
}*/

Object Storage::getObjectFromY(string key) {
    string command = "HMGET " + key + " " + v(x) + " " + v(y) + " " + v(content);
    reply = (redisReply *) redisCommand(c,command.c_str());

    assert(reply->elements == 3);
    //Object(NodeId id_,double x_, double y_, string content_)
    if( reply->element[0]->str == NULL || reply->element[0]->str == "" || reply->element[1]->str == NULL || reply->element[1]->str == "" || reply->element[2]->str == NULL || reply->element[2]->str == "" )
    {
        cout << "haha" << endl;
        //Object ret( getSubId(key), atof(reply->element[0]->str), atof(reply->element[1]->str), reply->element[2]->str );
        Object ret( getSubId(key), 1, 1, "ss");
        freeReplyObject(reply);
        return ret;
    }
    else
    {
        Object ret( getSubId(key), atof(reply->element[0]->str), atof(reply->element[1]->str), reply->element[2]->str );
        freeReplyObject(reply);
        return ret;
    }
}

bool Storage::setObjectFromY(Object& object, NodeId belongTo) {
    string command = "HMSET " + belongTo + NODE_SPLITTER + object.id + " " + v(x) + " " + to_string(object.x) + " " + v(y) + " " + to_string(object.y) + " " + v(content)  + " " + object.content;
    reply = (redisReply *) redisCommand(c,command.c_str());

    bool ret = (reply->type != REDIS_REPLY_ERROR);
    freeReplyObject(reply);

    return ret;
}

string Storage::getUuid() {
    string command = "INCR ";
    command = command  + "WANGERDANUUID";
    reply = (redisReply *) redisCommand(c,command.c_str());
    return to_string(reply->integer);
}

bool Storage::deleteNode(string key) {
    /*    HDEL id field1 value1 [field2 value2 ]*/
    string command = "DEL " + key;
    reply = (redisReply *) redisCommand(c,command.c_str());

    bool ret = (reply->type != REDIS_REPLY_ERROR);
    freeReplyObject(reply);

    return ret;
}

bool Storage::flushAll() {
    string command = "FLUSHALL";
    reply = (redisReply *) redisCommand(c,command.c_str());

    bool ret = (reply->type != REDIS_REPLY_ERROR);
    freeReplyObject(reply);

    return ret;
}

