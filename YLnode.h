//
// Created by houyx on 1/31/17.
//
#pragma once
#ifndef INC_2DINDEXTREE_YLNODE_H
#define INC_2DINDEXTREE_YLNODE_H

#include "Node.h"

static int leftLessEqual,leftBiggerEqual,rightLessEqual,rightBiggerEqual,next,pre;

class YLnode :public Node
{

public:
    NodeId belongTo;

    YLnode( Storage& storage_, NodeId belongTo_, NodeId id):belongTo(belongTo_),Node( storage_,belongTo_+NODE_SPLITTER+id )
    {
    }

    YLnode( Storage& storage_, NodeId id, int mark ):belongTo(getBelongToId(id)),Node( storage_,id )
    {
    }

    void resetWithFullId( NodeId _id )
    {
        belongTo = getBelongToId(_id);
        id = _id;
    }

    //todo: optimize multi get
    YLnode( Storage& storage_, NodeId belongTo_, Object object ):belongTo(belongTo_),Node( storage_,belongTo_+NODE_SPLITTER+object.id )
    {
        //values.clear();

        //todo nothing todo with map?
        storage.setObjectFromY(object,belongTo_);
/*
        setValue(v(x),to_string(object.x));
        setValue(v(y),to_string(object.y));
        setValue(v(content), object.content);*/
    }


    YLnode(const YLnode& c4):Node(c4.storage,c4.id),belongTo(c4.belongTo)
    {

    }

    YLnode& operator=(const YLnode&c4)
    {
        id = c4.id;
        belongTo = c4.belongTo;
        return *this;
    }

    bool operator==(const YLnode &c4);

    bool resetFromObject(NodeId belongTo_, Object object);

    //NodeId
    NodeId getLeftLessEqualId();

    NodeId getLeftBiggerEqualId();

    NodeId getRightLessEqualId();

    NodeId getRightBiggerEqualId();

    bool setLeftLessEqualId( NodeId a );

    bool setLeftBiggerEqualId( NodeId a );

    bool setRightLessEqualId( NodeId a );

    bool setRightBiggerEqualId( NodeId a );

    YLnode getNext();

    YLnode getPre();

    NodeId getNextId();

    NodeId getPreId();

    bool setNext( NodeId a );

    bool setPre( NodeId a );

    double getY();

    double getX();

    Object getObject();

    bool isNull();


};


#endif //INC_2DINDEXTREE_YLNODE_H
