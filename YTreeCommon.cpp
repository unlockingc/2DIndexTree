//
// Created by houyx on 2/4/17.
//

#include "YTreeCommon.h"

//
// Created by houyx on 1/25/17.
//
#include "TreeCommon.h"



/*#define BalFac(x) (stature((x).lChild) - stature((x).rChild)) //平衡因子
#define AvlBalanced(x) ((-2 < BalFac(x)) && (BalFac(x) < 2)) //AVL平衡条件*/


bool balancedY(YNode& g)
{
    if(g.isNull())
    {
        return true;
    }

    int temp = g.getLeftChild().getHeight() - g.getRightChild().getHeight();
    return temp > -2 && temp < 2;
}

void updateHeightY(YNode& g)
{
    if(g.isNull())
    {
        g.setHeight(0);
        return;
    }
    int tempLeft = g.getLeftChild().getHeight();
    int tempRight = g.getRightChild().getHeight();

    g.setHeight( tempLeft > tempRight? tempLeft + 1 : tempRight + 1 );
}


/**
 * todo: consider relative y tree reconstruct
 * @param a
 * @param b
 * @param c
 * @param t0
 * @param t1
 * @param t2
 * @param t3
 * @return
 */


YNode connect34Y(YNode& a,YNode& b,YNode& c,YNode t0,YNode t1, YNode t2, YNode t3)
{
    a.setLeftChild(t0);
    if(!t0.isNull())
    {
        t0.setParent(a);
    }
    a.setRightChild(t1);
    if(!t1.isNull())
    {
        t1.setParent(a);
    }

    updateHeightY(a);

    c.setLeftChild(t2);
    if( !t2.isNull() )
    {
        t2.setParent(c);
    }
    c.setRightChild(t3);
    if( !t3.isNull() )
    {
        t3.setParent(c);
    }

    updateHeightY(c);

    b.setLeftChild(a);
    a.setParent(b);
    b.setRightChild(c);
    c.setParent(b);

    updateHeightY(b);


    return b;
}

YNode rotateAtY( YNode v )
{
    assert(!v.isNull());

    YNode p = v.getParent(); YNode g = p.getParent();
    YNode temp = g.getParent();

    if( p.isLeftChild() )
    {
        if( v.isLeftChild() )
        {
            p.setParent( temp );

            return connect34Y(v, p, g,
                              v.getLeftChild(),
                              v.getRightChild(),
                              p.getRightChild(),
                              g.getRightChild());
        }
        else
        {
            v.setParent( temp );
            return connect34Y(p, v, g,
                              p.getLeftChild(),
                              v.getLeftChild(),
                              v.getRightChild(),
                              g.getRightChild());
        }
    }
    else
    {
        if( v.isRightChild() )
        {
            p.setParent( temp );
            return connect34Y(g, p, v,
                              g.getLeftChild(),
                              p.getLeftChild(),
                              v.getLeftChild(),
                              v.getRightChild());
        }
        else
        {
            v.setParent( temp );
            return connect34Y(g, v, p,
                              g.getLeftChild(),
                              v.getLeftChild(),
                              v.getRightChild(),
                              p.getRightChild());
        }
    }
}

YNode tallerChildY( YNode a )
{
    YNode leftChild = a.getLeftChild();
    YNode rightChild = a.getRightChild();


    if( leftChild.getHeight() > rightChild.getHeight() )
    {
        return leftChild;
    }
    else
    {
        if( leftChild.getHeight() < rightChild.getHeight() )
        {
            return rightChild;
        }
        else
        {
            if( a.isLeftChild() )//return the same side
            {
                return leftChild;
            }
            else
            {
                return rightChild;
            }
        }
    }
}


/*template <typename T> BinNodePosi(T) BinNode<T>::succ() { //定位节点v的直接后继
    BinNodePosi(T) s = this; //记录后继的临时变量
    if (rChild) { //若有右孩子，则直接后继必在右子树中，具体地就是
        s = rChild; //右子树中
        while (HasLChild(*s)) s = s->lChild; //最靠左（最小）的节点
    } else { //否则，直接后继应是“将当前节点包含于其左子树中的最低祖先”，具体地就是
        while (IsRChild(*s)) s = s->parent; //逆向地沿右向分支，不断朝左上方移动
        s = s->parent; //最后再朝右上方移动一步，即抵达直接后继（如果存在）
    }
    return s;
}*/


/***
 * can be NULL means it don't have next
 * @param a
 * @return
 */
YNode findNextY(YNode a)
{
    YNode ret = a;
    YNode s = a.getRightChild();
    if( !s.isNull() )
    {
        while( !s.isNull() )
        {
            ret.id = s.id;
            s = s.getLeftChild();
        }
    }
    else
    {
        //debug
        bool test = ret.isRightChild();
        while( ret.isRightChild() )
        {
            ret = ret.getParent();
        }
        ret = ret.getParent();
    }
    return  ret;
}


/***
 * keep parent and children,swap object
 * because id is swapped,so keep object, swap parent and children
 * and parent-children's pointer need to be fixed too
 * @param a
 * @param b
 */
void swapData( YNode& a, YNode& b )
{
    if( a.getParent() == b )
    {
        bool bisLeftChild = b.isLeftChild(), aisLeftChild = a.isLeftChild();
        YNode temp = b.getParent(), bOr = aisLeftChild? b.getRightChild():b.getLeftChild(), aL = a.getLeftChild(), aR = a.getRightChild();

        a.setParent(temp);
        if(!temp.isNull())
        {
            bisLeftChild? temp.setLeftChild(a):temp.setRightChild(a);
        }

        aisLeftChild? a.setRightChild(bOr):a.setLeftChild(bOr);
        if(!bOr.isNull()) {
            bOr.setParent(a);
        }

        aisLeftChild? a.setLeftChild(b):a.setRightChild(b);
        b.setParent(a);

        b.setLeftChild(aL);
        if(!aL.isNull())
        {
            aL.setParent(b);
        }

        b.setRightChild(aR);
        if(!aR.isNull())
        {
            aR.setParent(b);
        }
    }
    else if( b.getParent() == a )
    {
        bool bisLeftChild = b.isLeftChild(), aisLeftChild = a.isLeftChild();
        YNode temp = a.getParent(), aOr = bisLeftChild? a.getRightChild():a.getLeftChild(), bL = b.getLeftChild(), bR = b.getRightChild();

        b.setParent(temp);
        if(!temp.isNull())
        {
            aisLeftChild? temp.setLeftChild(b):temp.setRightChild(b);
        }

        bisLeftChild? b.setRightChild(aOr):b.setLeftChild(aOr);
        if(!aOr.isNull()) {
            aOr.setParent(b);
        }

        bisLeftChild? b.setLeftChild(a):b.setRightChild(a);
        a.setParent(b);

        a.setLeftChild(bL);
        if(!bL.isNull())
        {
            bL.setParent(a);
        }

        a.setRightChild(bR);
        if(!bR.isNull())
        {
            bR.setParent(a);
        }
    }
    else
    {

        bool bisLeftChild = b.isLeftChild(), aisLeftChild = a.isLeftChild();
        YNode tempa = a.getParent(),tempb = b.getParent(), aL = a.getLeftChild(), aR = a.getRightChild(), bL = b.getLeftChild(), bR = b.getRightChild();

        a.setParent(tempb);
        if(!tempb.isNull())
        {
            bisLeftChild? tempb.setLeftChild(a):tempb.setRightChild(a);
        }

        b.setParent(tempa);
        if(!tempa.isNull())
        {
            aisLeftChild? tempa.setLeftChild(b):tempa.setRightChild(b);
        }

        a.setLeftChild(bL);
        if(!bL.isNull())
        {
            bL.setParent(a);
        }

        a.setRightChild(bR);
        if(!bR.isNull())
        {
            bR.setParent(a);
        }

        b.setLeftChild(aL);
        if(!aL.isNull())
        {
            aL.setParent(b);
        }

        b.setRightChild(aR);
        if(!aR.isNull())
        {
            aR.setParent(b);
        }
    }

    int temp1 = b.getHeight();
    b.setHeight(a.getHeight());
    a.setHeight(temp1);
}

/***
 * can be NULL means it don't have pre
 * @param a
 * @return
 */
YNode findPreY(YNode a)
{
    YNode ret = a;
    YNode s = a.getLeftChild();
    if( !s.isNull() )
    {
        while( !s.isNull() )
        {
            ret.id = s.id;
            s = s.getRightChild();
        }
    }
    else
    {
        while( ret.isLeftChild() )
        {
            ret = ret.getParent();
        }
        ret = ret.getParent();
    }
    return  ret;
}
