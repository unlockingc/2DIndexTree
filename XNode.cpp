//
// Created by houyx on 1/23/17.
//
#include "XNode.h"

using namespace std;





XNode::XNode(Storage &storage_, Object object): Node( storage_,object.id )
{
    //values.clear();
    setValue(v(x),to_string(object.x));

    //information below is necessary for x tree
    /*
    setValue(v(y),to_string(object.y));
    setValue(v(content), object.content);*/
}

Object XNode::getObject() {
    //debug
    double out = atof(getValue(v(y)).c_str());
    string out2 = getValue(v(content));

    return Object( id, atof(getValue(v(x)).c_str()),atof(getValue(v(y)).c_str()),getValue(v(content)));
}

int XNode::getHeight() {
    string temp = getValue(v(height));
    return temp == "NULL"? 0: atoi(temp.c_str());
}

XNode XNode::getRightChild() {

    return XNode(storage,getValue(v(rightChild)));
}

XNode XNode::getLeftChild() {
    return XNode(storage,getValue(v(leftChild)));
}

XNode XNode::getParent() {
    return XNode(storage,getValue(v(parent)));
}



bool XNode::isLeftChild() {
    if( isRoot() )
    {
        return false;
    }
    else
    {
        return (getParent().getLeftChild() == *this);
    }
}
bool XNode::isRightChild() {
    if( isRoot() )
    {
        return false;
    }
    else
    {
        return (getParent().getRightChild() == *this);
    }
}

bool XNode::isRoot() {
    return getParent().isNull();
}

bool XNode::operator==(const XNode &c4)
{
    return id==c4.id;
}

double XNode::getX() {
    return atof(getValue(v(x)).c_str());
}


double XNode::getY() {
    return atof(getValue(v(y)).c_str());
}


bool XNode::setLeftChild( XNode& a )
{
    return setValue(v(leftChild),a.id);
}

bool XNode::setRightChild( XNode& a )
{
    return setValue(v(rightChild),a.id);
}

bool XNode::setParent( XNode& a )
{
    return setValue(v(parent),a.id);
}

bool XNode::setHeight(int h)
{
    return setValue(v(height), to_string(h) );
}

bool XNode::setYTail(NodeId Yid) {
    return setValue(v(yTail),Yid);
}

bool XNode::setYHeader(NodeId yId) {
    return setValue(v(yHeader),yId);
}

bool XNode::setYListMark(NodeId mark) {
    return setValue(v(yListMark),mark);
}

NodeId XNode::getYListMark() {
    return getValue(v(yListMark));
}

NodeId XNode::getYTail() {
    return getValue(v(yTail));
}

NodeId XNode::getYHeader() {
    return getValue(v(yHeader));
}

bool XNode::hasLeftChild() {
    return getValue(v(leftChild)) != NULL_REDIS;
}

bool XNode::hasRightChild() {
    return getValue(v(rightChild)) != NULL_REDIS;
}

bool XNode::hasParent() {
    return getValue(v(leftChild)) != NULL_REDIS;
}
