//
// Created by houyx on 1/24/17.
//
#pragma once
#ifndef INC_2DINDEXTREE_YLIST_H
#define INC_2DINDEXTREE_YLIST_H

#include "TreeCommon.h"

static int parents,child,number;


class YList {

private:
    Storage& storage;

public:
    XNode belongTo;
    YLnode header,tail;
    bool isLeftChild;
    YList( Storage& storage_,  XNode& belongTo_, NodeId header_, NodeId tail_, bool isLeftChild_ ): isLeftChild(isLeftChild_), belongTo(belongTo_),storage(storage_),header(storage_, header_,1),tail(storage_, tail_,1)
    {

    }



    Neighbor insert( Object& object, Neighbor& neighbor );//only fix parent

    void fixParentByLeft(NodeId preInParent, NodeId nextInParent, NodeId &myId, NodeId &target);

    void fixParentByRight(NodeId preInParent, NodeId nextInParent, NodeId &myId, NodeId &target);

    Neighbor remove(Object &object, Neighbor &neighbor);

    void removeFixParentByLeft(NodeId preId, NodeId nextId, YLnode &parentPre, YLnode &parentNext, NodeId preInParent,
                                   NodeId nextInParent);

    void removeFixParentByRight(NodeId preId, NodeId nextId, YLnode &parentPre, YLnode &parentNext, NodeId preInParent,
                                NodeId nextInParent);

    void resetByXNode( XNode& a );






};


#endif //INC_2DINDEXTREE_YLIST_H
