//
// Created by houyx on 1/23/17.
//
#pragma once
#ifndef INC_2DINDEXTREE_TREEINTERFACE_H
#define INC_2DINDEXTREE_TREEINTERFACE_H

#include "XTree.h"




class TreeInterface {

private:
    XTree xTree;
    Storage& storage;

public:
    TreeInterface( Storage& storage_ ):storage(storage_),xTree(storage_)
    {
        storage.getUuid();
    }

    Object insert( Object& obj );

    bool remove( NodeId& id ); //remove can do without search, make it quicker

    Object search( NodeId& id ); //search can be a small range, make it can bie ignored temporarily

    vector<Object> rangeQuery(  Range& range );
};


#endif //INC_2DINDEXTREE_TREEINTERFACE_H
