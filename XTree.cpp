//
// Created by houyx on 1/23/17.
//

#include "XTree.h"





/*
 *    if (!v || (e == v->data)) return v; //递归基：在节点v（或假想的通配节点）处命中
   hot = v; //一般情况：先记下当前节点，然后再
   return searchIn(((e < v->data) ? v->lChild : v->rChild), e, hot); //深入一层，递归查找
 * */

XNode XTree::_searchIn( XNode& rootIn ,XNode& newNode, XNode& hot) {

    if( rootIn.isNull() || rootIn.getX() == newNode.getX() )//todo : must estimate x, otherwise insert will be wrong
    {
        return rootIn;
    }

    hot.id = rootIn.id;
    (rootIn.getX() > newNode.getX())? rootIn.id = rootIn.getValue(v(leftChild)) : rootIn.id = rootIn.getValue(v(rightChild)); //this make map useless
    return _searchIn( rootIn,newNode,hot);
}


XNode XTree::_search(XNode& newNode) {
    XNode temp(root);
    return _searchIn(temp, newNode, _hot);
}

void XTree::_insertInYList(Neighbor &neighbor, Object &object)
{
    XNode now(storage,(root.getX() > object.x)? root.getValue(v(leftChild)) : root.getValue(v(rightChild)));

    YList list(storage,now,now.getYHeader(),now.getYTail(),now.isLeftChild());
    _insertInYListIn(now,neighbor,object,list);
}

void XTree::_insertInYListIn( XNode& now, Neighbor& neighbor, Object& object, YList& list )
{
    neighbor = list.insert(object,neighbor);
    if( now.id == object.id )
    {
        return;
    }
    else
    {
        (now.getX() > object.x)? now.id = now.getValue(v(leftChild)) : now.id = now.getValue(v(rightChild)); //this make map useless
        list.resetByXNode(now);
        return _insertInYListIn( now,neighbor,object,list );
    }
}


/*
 *    BinNodePosi(T) & x = search(e); if (x) return x; //确认目标节点不存在（留意对_hot的设置）
   x = new BinNode<T>(e, _hot); _size++; //创建节点x（此后，其父_hot可能增高，祖父可能失衡）
   for (BinNodePosi(T) g = _hot; g; g = g->parent) { //从x之父出发向上，逐层检查各代祖先g
      if (!AvlBalanced(*g)) { //一旦发现g失衡，则（采用“3 + 4”算法）使之复衡
         FromParentTo(*g) = rotateAt(tallerChild(tallerChild(g))); //将该子树联至原父亲
         break; //g复衡后，局部子树高度必然复原；其祖先亦必如此，故调整随即结束
      } else //否则（g依然平衡），只需简单地
         updateHeight(g); //更新其高度（注意：即便g未失衡，高度亦可能增加）
   } //至多只需一次调整；若果真做过调整，则全树高度必然复原
   return x; //返回新节点
 * */

/*#define FromParentTo(x) ( \
   IsRoot(x) ? _root : ( \
   IsLChild(x) ? (x).parent->lChild : (x).parent->rChild \
   ) \
)*/
Object XTree::insert(Object& object) {
    XNode newNode( storage,object );
    XNode x = _search(newNode);

    if( !x.isNull() )
    {
        //todo: delete getObject()
        return object;
    }

    newNode.setValue(v(parent),_hot.id);
    newNode.setValue(v(height),"1");

    //debug
    //string YListMark = storage.getUuid();
    newNode.setYListMark(newNode.id);


    _hot.setValue( (_hot.getX() > object.x )?v(leftChild):v(rightChild), newNode.id );

    //insert in ytree
    Neighbor neighbor = yRoot.insert( object );
    newNode.setYHeader(newNode.id+NODE_SPLITTER+newNode.id);
    newNode.setYTail(newNode.id+NODE_SPLITTER+newNode.id);
    //insert in yList from bottom to top, return per and next as a reference to insert in parent's yList
    _insertInYList( neighbor, object );

    XNode parentTemp(root);
    //adjust xtree
    for(  XNode g(_hot); !g.isNull(); g.id = g.getValue(v(parent)) )
    {
        if( !balanced(g) )
        {
            parentTemp.id = g.getValue(v(parent));

            XNode temp = rotateAt( tallerChild( tallerChild(g)));


            if( !parentTemp.isNull() )
            {
                parentTemp.setValue(( parentTemp.getX() >  g.getX() )?v(leftChild):v(rightChild), temp.id);
            }
            else
            {
                //update root
                root = temp;
                yRoot.belongTo = temp;
            }
            break;
        }
        else
        {
            updateHeight(g);
        }
    }

    return object;
}

/*template <typename T> bool AVL<T>::remove(const T & e) { //从AVL树中删除关键码e
    BinNodePosi(T) & x = search(e); if (!x) return false; //确认目标节点存在（留意对_hot的设置）
    removeAt(x, _hot); _size--; //先按BST规则删除之（此后，原节点之父_hot及其祖先均可能失衡）
    for (BinNodePosi(T) g = _hot; g; g = g->parent) { //从_hot出发向上，逐层检查各代祖先g
        if (!AvlBalanced(*g)) //一旦发现g失衡，则（采用“3 + 4”算法）使之复衡
            g = FromParentTo(*g) = rotateAt(tallerChild(tallerChild(g))); //将该子树联至原父亲
        updateHeight(g); //并更新其高度（注意：即便g未失衡，高度亦可能降低）
    } //可能需做Omega(logn)次调整――无论是否做过调整，全树高度均可能降低
    return true; //删除成功
} //若目标节点存在且被删除，返回true；否则返回false*/

bool XTree::remove(NodeId& id)
{
    XNode newNode( storage, id );
    XNode x = _search(newNode);

    if( x.isNull() )
    {
        //todo: delete getObject()
        return false;
    }


    //remove in Ylist

    YNode temp(storage,id);
    Object object = temp.getObject();
    Neighbor neighbor = yRoot.remove( id );
    _removeInYList( neighbor, object );

    //remove in xTree, be careful for succ swap changing the Ylist
    removeAt(x, _hot);




    XNode parentTemp(root);
    //adjust YTree
    for(  XNode g(_hot); !g.isNull(); g.id = g.getValue(v(parent)) )
    {
        if( !balanced(g) )
        {
            parentTemp.id = g.getValue(v(parent));

            XNode temp = rotateAt( tallerChild( tallerChild(g)));

            if( !parentTemp.isNull() )
            {
                parentTemp.setValue(( parentTemp.getX() >  g.getX() )?v(leftChild):v(rightChild), temp.id);
            }
            else
            {
                //update root
                root = temp;
                yRoot.belongTo = temp;
            }

            g = temp;
        }
        updateHeight(g);
    }

    return true;

}

/***
 * todo: note that x is already deleted
 * @param x
 * @param hot
 */
void XTree::removeAt( XNode& x, XNode& hot )
{
    bool isXRoot = (root.id == x.id);
    XNode w = x;
    XNode succ = w;
    bool isWLeftchild;

    if(!x.hasLeftChild())
    {
        succ = x.getRightChild();


        //never can be null because the initRoot
        if(isXRoot)
        {
            root = succ;
        }

        hot = w.getParent();
        isWLeftchild = w.isLeftChild();
        fixListPointer( w, succ );
    }
    else if(!x.hasRightChild())
    {
        succ = x.getLeftChild();

        if(isXRoot)
        {
            root = succ;
        }

        hot = w.getParent();
        isWLeftchild = w.isLeftChild();
        fixListPointer( w, succ );
    }
    else
    {
        w = findNext(w);


        if(isXRoot)
        {
            root = w;
        }

        succ = w.getRightChild();
        isWLeftchild = w.isLeftChild();

        swapDataX( x, w );
        deleteWFromWtoX(x, w);

        w = x;
        fixListPointer(w, succ);


        //get parent after swapped
        hot = w.getParent();

        //(u == x) ? u.setRightChild(succ):u.setLeftChild(succ);//wrong!!!!fix
    }


    isWLeftchild?hot.setLeftChild(succ):hot.setRightChild(succ);
    if(!succ.isNull())
    {
        succ.setParent(hot);
    }
    w.deleteSelf();
    //insert in yList from bottom to top, return per and next as a reference to insert in parent's yList

    //insert in ytree


}

vector<Object> XTree::range(Range& range) {
    vector<Object> ret;

    assert(range.xMin!=range.xMax && range.yMin!= range.yMax);

    Object min("NotInTreeRangeMin",range.xMin,range.yMin,"hehe");
    Object max("NotInTreeRangeMax",range.xMax,range.yMax,"hehe");

    YLnode yMin = yRoot.getYLess(min,max);
    YLnode yMax = yRoot.getYBigger(min, max);
    XNode temp(root);

    _rangeSearchTogether(yMin, yMax, min, max, temp, ret);

    return ret;
}

void XTree::_removeInYList(Neighbor &neighbor, Object &object) {

    if(root.id == object.id)
    {
        return;
    }

    XNode now(storage,(root.getX() > object.x)? root.getValue(v(leftChild)) : root.getValue(v(rightChild)));

    YList list(storage,now,now.getYHeader(),now.getYTail(),now.isLeftChild());
    _removeInYListIn(now, neighbor, object, list);


}

void XTree::_removeInYListIn(XNode &now, Neighbor &neighbor, Object &object, YList &list) {

    neighbor = list.remove(object,neighbor);
    if( now.id == object.id )
    {
        return;
    }
    else
    {
        (now.getX() > object.x)? now.id = now.getValue(v(leftChild)) : now.id = now.getValue(v(rightChild)); //this make map useless
        list.resetByXNode(now);
        return _removeInYListIn( now,neighbor,object,list );
    }

}


/**
 * even if two path is not same length, the program is still right.
 * @param min
 * @param yMax
 * @param range
 * @param rootIn
 */
void XTree::_rangeSearchTogether(YLnode &yMin, YLnode &yMax, Object &min, Object &max, XNode &rootIn, vector<Object> &result) {
    XNode minNext( rootIn );
    XNode maxNext( rootIn );

    YLnode yMin1(yMin);
    YLnode yMax1(yMax);


    NodeId temp;

    while( minNext == maxNext && (!minNext.isNull()) )
    {

        assert( yMin.belongTo == yMax.belongTo || yMin.isNull() || yMax.isNull() );
        assert( yMin1.belongTo == yMax1.belongTo || yMin1.isNull() || yMax1.isNull() );

        //check onPath node is in result?
        if( minNext.getX() <= max.x && minNext.getX() >= min.x )
        {
            YNode tempY(storage,minNext.id);
            if( !tempY.isNull() && tempY.getY() <= max.y && tempY.getY() >= min.y )
            {
                result.push_back(tempY.getObject());
            }
        }

        if( minNext.getX() == min.x )
        {
            yMin1.resetWithFullId( (maxNext.getX() > max.x)? yMin1.getLeftBiggerEqualId():yMin1.getRightBiggerEqualId() );
            yMax1.resetWithFullId( (maxNext.getX() > max.x)? yMax1.getLeftLessEqualId():yMax1.getRightLessEqualId() );

            (maxNext.getX() > max.x)? maxNext.id = maxNext.getValue(v(leftChild)) : maxNext.id = maxNext.getValue(v(rightChild));

            _rangeSearchInMaxPath( yMin1, yMax1, result, maxNext,min,max );
            return;
        }

        if( maxNext.getX() == max.x )
        {
            yMin.resetWithFullId( (minNext.getX() > min.x)? yMin.getLeftBiggerEqualId():yMin.getRightBiggerEqualId() );
            yMax.resetWithFullId( (minNext.getX() > min.x)? yMax.getLeftLessEqualId():yMax.getRightLessEqualId() );

            (minNext.getX() > min.x)? minNext.id = minNext.getValue(v(leftChild)) : minNext.id = minNext.getValue(v(rightChild));

            _rangeSearchInMinPath( yMin, yMax, result, minNext,min,max );
            return;
        }

        yMin.resetWithFullId( (minNext.getX() > min.x)? yMin.getLeftBiggerEqualId():yMin.getRightBiggerEqualId() );
        yMax.resetWithFullId( (minNext.getX() > min.x)? yMax.getLeftLessEqualId():yMax.getRightLessEqualId() );

        (minNext.getX() > min.x)? minNext.id = minNext.getValue(v(leftChild)) : minNext.id = minNext.getValue(v(rightChild));

        yMin1.resetWithFullId( (maxNext.getX() > max.x)? yMin1.getLeftBiggerEqualId():yMin1.getRightBiggerEqualId() );
        yMax1.resetWithFullId( (maxNext.getX() > max.x)? yMax1.getLeftLessEqualId():yMax1.getRightLessEqualId() );

        (maxNext.getX() > max.x)? maxNext.id = maxNext.getValue(v(leftChild)) : maxNext.id = maxNext.getValue(v(rightChild));

    }

    if( !minNext.isNull() )
    {
        _rangeSearchInMinPath( yMin, yMax, result, minNext,min,max );
    }

    if( !maxNext.isNull() )
    {
        _rangeSearchInMaxPath( yMin1, yMax1, result, maxNext,min,max );
    }



}

void XTree::_rangeSearchInMinPath(YLnode &yMin, YLnode &yMax, vector<Object> &result, XNode &rootIn, Object &min,
                                  Object &max) {
    double tempx;
    YLnode tempyMin(yMin);
    YLnode tempyMax(yMax);
    XNode tempRightChild(rootIn);

    while(  (tempx = rootIn.getX())!=min.x && !rootIn.isNull() )
    {
        assert( yMin.belongTo == yMax.belongTo || yMin.isNull() || yMax.isNull() );
        //check onPath node is in result?
        if( tempx <= max.x && tempx >= min.x )
        {
            YNode tempY(storage,rootIn.id);
            if(  !tempY.isNull() && tempY.getY() <= max.y && tempY.getY() >= min.y )
            {
                result.push_back(tempY.getObject());
            }
        }

        tempRightChild = rootIn.getRightChild();
        if( !tempRightChild.isNull() && (tempx > min.x) )
        {
            tempyMin.resetWithFullId(yMin.getRightBiggerEqualId());
            tempyMax.resetWithFullId(yMax.getRightLessEqualId());

            if( !tempyMin.isNull() ) {
                while (!(tempyMin == tempyMax)) {
                    result.push_back(tempyMin.getObject());
                    tempyMin.id = tempyMin.getNextId();
                }
                if (!tempyMax.isNull()) {
                    result.push_back(tempyMax.getObject());
                }
            } else
            {
                if( !tempyMax.isNull() )
                {
                    while (!(tempyMin == tempyMax)) {
                        result.push_back(tempyMax.getObject());
                        tempyMax.id = tempyMax.getPreId();
                    }
                    if (!tempyMax.isNull()) {
                        result.push_back(tempyMax.getObject());
                    }
                }
            }
        }

        yMin.resetWithFullId( (tempx > min.x)? yMin.getLeftBiggerEqualId():yMin.getRightBiggerEqualId() );
        yMax.resetWithFullId( (tempx > min.x)? yMax.getLeftLessEqualId():yMax.getRightLessEqualId() );

        (tempx > min.x)? rootIn.id = rootIn.getValue(v(leftChild)) : rootIn.id = rootIn.getValue(v(rightChild));
    }
}

void XTree::_rangeSearchInMaxPath(YLnode &yMin, YLnode &yMax, vector<Object> &result, XNode &rootIn, Object &min,
                                  Object &max) {

    double tempx;
    YLnode tempyMin(yMin);
    YLnode tempyMax(yMax);
    XNode tempLeftChild(rootIn);



    while(  (tempx = rootIn.getX())!=max.x && !rootIn.isNull() )
    {
        assert( yMin.belongTo == yMax.belongTo || yMin.isNull() || yMax.isNull() );
        //check onPath node is in result?
        if( tempx <= max.x && tempx >= min.x )
        {
            YNode tempY(storage,rootIn.id);
            if(  !tempY.isNull() && tempY.getY() <= max.y && tempY.getY() >= min.y )
            {
                result.push_back(tempY.getObject());
            }
        }

        tempLeftChild = rootIn.getLeftChild();
        if( !tempLeftChild.isNull() && (tempx < max.x) )
        {
            tempyMin.resetWithFullId(yMin.getLeftBiggerEqualId());
            tempyMax.resetWithFullId(yMax.getLeftLessEqualId());

            if( !tempyMin.isNull() ) {
                while (!(tempyMin == tempyMax)) {
                    result.push_back(tempyMin.getObject());
                    tempyMin.id = tempyMin.getNextId();
                }
                if (!tempyMax.isNull()) {
                    result.push_back(tempyMax.getObject());
                }
            } else
            {
                if( !tempyMax.isNull() )
                {
                    while (!(tempyMin == tempyMax)) {
                        result.push_back(tempyMax.getObject());
                        tempyMax.id = tempyMax.getPreId();
                    }
                    if (!tempyMax.isNull()) {
                        result.push_back(tempyMax.getObject());
                    }
                }
            }
        }

        yMin.resetWithFullId( (tempx > max.x)? yMin.getLeftBiggerEqualId():yMin.getRightBiggerEqualId() );
        yMax.resetWithFullId( (tempx > max.x)? yMax.getLeftLessEqualId():yMax.getRightLessEqualId() );

        (tempx > max.x)? rootIn.id = rootIn.getValue(v(leftChild)) : rootIn.id = rootIn.getValue(v(rightChild));
    }
}



