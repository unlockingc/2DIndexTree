//
// Created by houyx on 1/31/17.
//

#include "YLnode.h"




NodeId YLnode::getLeftLessEqualId() {
    return getValue(v(leftLessEqual));
}

NodeId YLnode::getLeftBiggerEqualId() {
    return getValue(v(leftBiggerEqual));
}

NodeId YLnode::getRightLessEqualId() {
    return getValue(v(rightLessEqual));
}

NodeId YLnode::getRightBiggerEqualId() {
    return getValue(v(rightBiggerEqual));
}

bool YLnode::setLeftLessEqualId(NodeId a) {

    return setValue(v(leftLessEqual),a);
}

bool YLnode::setLeftBiggerEqualId(NodeId a) {
    return setValue(v(leftBiggerEqual),a);
}

bool YLnode::setRightLessEqualId(NodeId a) {
    return setValue(v(rightLessEqual),a);
}

bool YLnode::setRightBiggerEqualId(NodeId a) {
    return setValue(v(rightBiggerEqual),a);
}

bool YLnode::setNext(NodeId a) {
    return setValue(v(next),a);
}

bool YLnode::setPre(NodeId a) {
    return setValue(v(pre),a);
}

YLnode YLnode::getNext() {
    return YLnode(storage,getValue(v(next)),1);
}

YLnode YLnode::getPre() {
    return YLnode(storage,getValue(v(pre)),1);
}

NodeId YLnode::getNextId() {
    return getValue(v(next));
}

NodeId YLnode::getPreId() {
    return getValue(v(pre));
}

double  YLnode::getY()
{
    return atof(getValue(v(y)).c_str());
}

double YLnode::getX()
{
    return atof(getValue(v(x)).c_str());
}


//todo: optimize multi get
Object YLnode::getObject() {
    return storage.getObjectFromY(id);
}

bool YLnode::resetFromObject(NodeId belongTo_, Object object)
{
    belongTo = belongTo_;
    id = belongTo_ + NODE_SPLITTER + object.id;
    storage.setObjectFromY(object,belongTo_);
}

bool YLnode::operator==(const YLnode &c4)
{
    return id == NULL_REDIS? c4.id == NULL_REDIS : (id==c4.id && belongTo == c4.belongTo);
}

bool YLnode::isNull() {
    //debug
    string ret = getSubId(id);

    return getSubId(id) == NULL_REDIS || id == NULL_REDIS;
}
